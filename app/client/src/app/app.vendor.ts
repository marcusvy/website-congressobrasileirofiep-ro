/**
 * D3
 */
import * as d3 from 'd3';

/**
 * Moment
 */
import * as moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-br');

/**
 * QuillEditor
 */
import * as Quill from 'quill';

/**
 * Hammer
 */
import 'hammerjs';
import 'hammer-time';

/**
 * WebFont
 */
import * as WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Roboto:100,500']
  }
});

export class AppVendor {}
