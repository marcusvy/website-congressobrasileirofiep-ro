import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiepMenuComponent } from './fiep-menu.component';

describe('FiepMenuComponent', () => {
  let component: FiepMenuComponent;
  let fixture: ComponentFixture<FiepMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiepMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiepMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
