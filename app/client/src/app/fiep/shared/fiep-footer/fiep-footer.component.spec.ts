import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiepFooterComponent } from './fiep-footer.component';

describe('FiepFooterComponent', () => {
  let component: FiepFooterComponent;
  let fixture: ComponentFixture<FiepFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiepFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiepFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
