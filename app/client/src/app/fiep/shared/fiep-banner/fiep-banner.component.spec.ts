import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiepBannerComponent } from './fiep-banner.component';

describe('FiepBannerComponent', () => {
  let component: FiepBannerComponent;
  let fixture: ComponentFixture<FiepBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiepBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiepBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
