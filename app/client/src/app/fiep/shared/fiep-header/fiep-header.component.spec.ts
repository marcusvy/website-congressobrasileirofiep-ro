import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiepHeaderComponent } from './fiep-header.component';

describe('FiepHeaderComponent', () => {
  let component: FiepHeaderComponent;
  let fixture: ComponentFixture<FiepHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiepHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiepHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
