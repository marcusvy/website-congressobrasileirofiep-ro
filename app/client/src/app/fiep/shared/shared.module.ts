import { FiepRoutingModule } from './../fiep-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './../../mv/core/core.module';
import { UiModule } from '../../mv/ui/ui.module';
import { FiepHeaderComponent } from './fiep-header/fiep-header.component';
import { FiepMenuComponent } from './fiep-menu/fiep-menu.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { FiepBannerComponent } from './fiep-banner/fiep-banner.component';
import { FiepFooterComponent } from './fiep-footer/fiep-footer.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SharedModule { }

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    UiModule,
    FiepRoutingModule,
  ],
  declarations: [
    PageTitleComponent,
    FiepHeaderComponent,
    FiepMenuComponent,
    FiepBannerComponent,
    FiepFooterComponent
  ],
  exports: [
    CoreModule,
    UiModule,
    ReactiveFormsModule,
    PageTitleComponent,
    FiepHeaderComponent,
    FiepFooterComponent
  ]
})
export class FiepSharedModule { }
