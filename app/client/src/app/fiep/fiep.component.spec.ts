import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiepComponent } from './fiep.component';

describe('FiepComponent', () => {
  let component: FiepComponent;
  let fixture: ComponentFixture<FiepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
