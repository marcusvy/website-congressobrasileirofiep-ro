import { SoonComponent } from './soon/soon.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiepComponent } from "./fiep.component";

import { HomeComponent } from './home/home.component';
import { ContatoComponent } from './contato/contato.component';
// import { CronogramaComponent } from './cronograma/cronograma.component';
// import { ApoioComponent } from './apoio/apoio.component';
// import { SubmissaoComponent } from './submissao/submissao.component';
// import { ComissaoComponent } from './comissao/comissao.component';
// import { ProgramacaoComponent } from './programacao/programacao.component';

// import { NormasComponent } from './normas/normas.component';
// import { InscricaoComponent } from './inscricao/inscricao.component';
// import { SimposioComponent } from './simposio/simposio.component';
// import { ComunicacaoComponent } from './comunicacao/comunicacao.component';
// import { MinicursoComponent } from './minicurso/minicurso.component';
// import { OuvinteComponent } from './ouvinte/ouvinte.component';
// import { OrientacaoComponent } from './orientacao/orientacao.component';
// import { EixosComponent } from './eixos/eixos.component';

const routes: Routes = [
  {
    path: '',
    component: FiepComponent,
    children: [
      { path: '', component: HomeComponent, pathMatch: 'full' },
      // { path: 'programacao', component: ProgramacaoComponent },
      // { path: 'submissao', component: SubmissaoComponent },
      // { path: 'apoio', component: ApoioComponent },
      // { path: 'cronograma', component: CronogramaComponent },
      { path: 'contato', component: ContatoComponent },
      // { path: 'comissao', component: ComissaoComponent },
      // { path: 'normas', component: NormasComponent },
      // { path: 'eixos', component: EixosComponent },
      // { path: 'inscricao', component: InscricaoComponent },
      // { path: 'simposio', component: SimposioComponent },
      // { path: 'comunicacao', component: ComunicacaoComponent },
      // { path: 'minicurso', component: MinicursoComponent },
      // { path: 'ouvinte', component: OuvinteComponent },
      // { path: 'orientacao', component: OrientacaoComponent },
      { path: '**', component: SoonComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FiepRoutingModule { }

