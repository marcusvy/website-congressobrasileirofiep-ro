import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiepSharedModule } from './shared/shared.module';
import { FiepRoutingModule } from './fiep-routing.module';

import { FiepComponent } from './fiep.component';
import { HomeComponent } from './home/home.component';
import { ContatoComponent } from './contato/contato.component';
import { SoonComponent } from './soon/soon.component';

@NgModule({
  imports: [
    CommonModule,
    FiepSharedModule,
    FiepRoutingModule,
  ],
  declarations: [
    FiepComponent,
    HomeComponent,
    ContatoComponent,
    SoonComponent
  ]
})
export class FiepModule { }

