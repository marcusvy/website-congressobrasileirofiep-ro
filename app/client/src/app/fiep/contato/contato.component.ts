import { Component, OnInit, Output, EventEmitter, HostBinding, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MV_ANIMATION_ROUTE_DEFAULT } from './../../mv/core/animation';

@Component({
  selector: 'mv-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.scss'],
  animations: [MV_ANIMATION_ROUTE_DEFAULT]
})
export class ContatoComponent implements OnInit {

  formEnabled = true;
  @HostBinding('@routeDefault')
  routeAnimation = true;

  public formContato: FormGroup;
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  send = false;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.formContato = this.fb.group({
      nome: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      mensagem: ['', Validators.required]
    });
  }

  enviar() {
    this.send = true;
    if (this.formContato.valid) {
      this.onSave.emit(this.formContato.value);
    }
  }

}
