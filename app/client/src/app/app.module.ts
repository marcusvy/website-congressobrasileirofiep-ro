import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
// import { LocationStrategy, HashLocationStrategy } from '@angular/common';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CoreModule} from './mv/core';
import { AuthModule, AuthService  } from './mv/auth';
import { ModelService } from './mv/ui';
import { SimposioService } from './mv/congresso/simposio/simposio.service';

/**
 * Aplication
 */
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    AuthModule,
    // MetodosFronteiricosModule,
    // CongressoModule,
  ],
  providers: [
    AuthService,
    ModelService,
    // SimposioService,
    { provide: LOCALE_ID, useValue: "pt-BR" },
    // { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
