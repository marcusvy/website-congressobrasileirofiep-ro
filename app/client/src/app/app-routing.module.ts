import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './mv/auth/auth.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: 'app/mv/auth/auth.module#AuthModule'
  },
  {
    path: 'mv/user',
    loadChildren: 'app/mv/user/user.module#UserModule'
  },
  {
    path: 'mv/congresso',
    loadChildren: 'app/mv/congresso/congresso-admin.module#CongressoAdminModule',
    canLoad: [AuthGuard],
  },
  {
    path: '',
    loadChildren: 'app/fiep/fiep.module#FiepModule'
    // children: []
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
