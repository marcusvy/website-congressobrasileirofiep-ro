import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'mv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'mv';

  theForm: FormGroup;
  tipos: Array<{ label: string, value: any }>

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {

    this.theForm = this.fb.group({
      'nome': ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(18),
      ])],
      'file':[],
      'hoje': ['2017-08-29'],
      'hora': [''],
      'agora': [''],
      'texto': [2],
      'tipo': [2],
      "email": ['email@gmail.com', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      'senha': [''],
      'search': [''],
      'telefone': [''],
      'url': [''],
      'toggle': [true],
      'radio':[],
      'check': []
    });
  }

}
