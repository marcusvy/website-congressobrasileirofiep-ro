import { PipeModule } from './pipe/pipe.module';
import { MasksModule } from './masks/masks.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UiModule } from '../ui/ui.module';
import { ConfigService } from './config.service';
import { ConnectionService } from './connection.service';
import { ApiHttpService } from './http/api-http.service';


import { CounterDirective } from './validator/counter.directive';

@NgModule({
  imports: [
    MasksModule,
    PipeModule
  ],
  declarations: [
    CounterDirective
  ],
  exports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    UiModule,
    MasksModule,
    PipeModule,
    CounterDirective
  ],
  providers: [
    ConfigService,
    ConnectionService,
    ApiHttpService,
  ],
})
export class CoreModule { }
