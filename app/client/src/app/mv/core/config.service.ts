import { Injectable } from '@angular/core';

import { environment } from './../../../environments/environment';

@Injectable()
export class ConfigService {

  // API base path: https://
  // public API = 'http://localhost:8000/api';
  // public API = 'https://api.metodosfronteiricos.com.br/api';
  public API = environment.API;

  constructor() { }

}
