import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaskNumberDirective } from './input/mask-number.directive';
import { MaskCPFDirective } from './input/mask-cpf.directive';
import { MaskAlphaDirective } from './input/mask-alpha.directive';
import { MaskMoneyDirective } from './input/mask-money.directive';
import { MaskDateDirective } from './input/mask-date.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MaskNumberDirective,
    MaskCPFDirective,
    MaskAlphaDirective,
    MaskMoneyDirective,
    MaskDateDirective
  ],
  exports: [
    MaskNumberDirective,
    MaskCPFDirective,
    MaskAlphaDirective,
    MaskMoneyDirective,
    MaskDateDirective
  ]
})
export class MasksModule { }
