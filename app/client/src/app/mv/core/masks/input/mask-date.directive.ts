import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

import * as VMasker from 'vanilla-masker';

@Directive({
  selector: '[mvInputMaskDate]'
})
export class MaskDateDirective {

  public nativeElement: HTMLInputElement;

  constructor(public element: ElementRef, public render: Renderer) {
    this.nativeElement = this.element.nativeElement;
    VMasker(this.nativeElement).maskPattern('99/99/9999');
  }

}
