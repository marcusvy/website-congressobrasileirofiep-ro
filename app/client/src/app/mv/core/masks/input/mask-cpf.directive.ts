import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

import * as VMasker from 'vanilla-masker';

@Directive({
  selector: '[mvInputMaskCPF]'
})
export class MaskCPFDirective {

  public nativeElement: HTMLInputElement;

  constructor(public element: ElementRef, public render: Renderer) {
    this.nativeElement = this.element.nativeElement;
    VMasker(this.nativeElement).maskPattern('999.999.999-99');
  }
}
