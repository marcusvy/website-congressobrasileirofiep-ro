import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

import * as VMasker from 'vanilla-masker';


@Directive({
  selector: '[mvInputMaskNumber]'
})
export class MaskNumberDirective {

  public nativeElement: HTMLInputElement;

  constructor(public element: ElementRef, public render: Renderer) {
    this.nativeElement = this.element.nativeElement;
    VMasker(this.nativeElement).maskNumber();
  }

}
