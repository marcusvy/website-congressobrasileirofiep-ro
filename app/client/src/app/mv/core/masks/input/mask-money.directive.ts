import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

import * as VMasker from 'vanilla-masker';

@Directive({
  selector: '[mvInputMaskMoney]'
})
export class MaskMoneyDirective {

  public nativeElement: HTMLInputElement;

  constructor(public element: ElementRef, public render: Renderer) {
    this.nativeElement = this.element.nativeElement;
    VMasker(this.nativeElement).maskMoney({
      // Decimal precision -> "90"
      precision: 2,
      // Decimal separator -> ",90"
      separator: ',',
      // Number delimiter -> "12.345.678"
      delimiter: '.',
      // Money unit -> "R$ 12.345.678,90"
      unit: 'R$',
      // Money unit -> "12.345.678,90 R$"
      // suffixUnit: 'R$',
      // Force type only number instead decimal,
      // masking decimals with ",00"
      // Zero cents -> "R$ 1.234.567.890,00"
      // zeroCents: true
    });
  }
}
