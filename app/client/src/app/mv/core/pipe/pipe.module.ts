import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoletoPipe } from './boleto/boleto.pipe';
import { BytesPipe } from './bytes/bytes.pipe';
import { CepPipe } from './cep/cep.pipe';
import { CnpjPipe } from './cnpj/cnpj.pipe';
import { CounterPipe } from './counter/counter.pipe';
import { CpfPipe } from './cpf/cpf.pipe';
import { DigitsPipe } from './digits/digits.pipe';
import { MomentPipe } from './moment/moment.pipe';
import { PhonePipe } from './phone/phone.pipe';
import { TimeAgoPipe } from './time-ago/time-ago.pipe';
import { TimeDifferencePipe } from './time-difference/time-difference.pipe';
import { TruncatePipe } from './truncate/truncate.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BoletoPipe,
    BytesPipe,
    CepPipe,
    CnpjPipe,
    CounterPipe,
    CpfPipe,
    DigitsPipe,
    MomentPipe,
    PhonePipe,
    TimeAgoPipe,
    TimeDifferencePipe,
    TruncatePipe,
  ],
  exports: [
    BoletoPipe,
    BytesPipe,
    CepPipe,
    CnpjPipe,
    CounterPipe,
    CpfPipe,
    DigitsPipe,
    MomentPipe,
    PhonePipe,
    TimeAgoPipe,
    TimeDifferencePipe,
    TruncatePipe,
  ],
  providers: [
    MomentPipe,
  ]
})
export class PipeModule { }
