export interface AbstractHttpService {

  storageSetItem(data);

  storageGetItem();

  fetch(id: number);

  fetchOnline(id: number);

  fetchStorage(id: any);

  fetchAll();

  fetchAllOnline();

  fetchAllStorage();

  create(entity: any);

  update(entity: any);

  delete(id: number);

  search(params: any);

  errorHandler(error: any);
}
