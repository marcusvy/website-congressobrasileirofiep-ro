export interface SearchParam {
  in: string;
  for: string;
}
