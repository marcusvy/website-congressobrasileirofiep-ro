import { trigger, state, animate, transition, style } from '@angular/animations';

export const MV_ANIMATION_TAB: any =
  trigger('tab', [
    state('active', style({ position: 'relative', opacity: 1, transform: 'translateY(0)' })),
    state('inactive', style({ opacity: 0, transform: 'translateY(10px)' })),

    transition('*<=>*', [
      animate('0.3s'),
    ]),

    // transition(':leave', [
    //   // animation and styles at end of transition
    //   animate('3s', style({ opacity: 0 })),
    // ]),



  ]);
