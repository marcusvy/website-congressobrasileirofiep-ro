import { Injectable, Inject } from '@angular/core';
import { Response, Http } from '@angular/http';
import { ApiHttpService } from '../core/http/api-http.service';
import { ConnectionService } from "../core/connection.service";

@Injectable()
export class UserService
  extends ApiHttpService {

  protected token: string = 'user::simposio';
  protected url: string = '/user';


  register(id: number, status: number) {
    let statusNumber = (status) ? 1 : 0;
    let url = `${this.api}/activation/${id}/${status}`;
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  authenticate(id: number, status: number) {
    let statusNumber = (status) ? 1 : 0;
    let url = `${this.api}/activation/${id}/${status}`;
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  activate(id: number, status: number) {
    let statusNumber = (status) ? 1 : 0;
    let url = `${this.api}/activation/${id}/${status}`;
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }
}
