import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'mv-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  results = [
    {
      "name": "On-line",
      "value": 30
    },
    {
      "name": "Off-line",
      "value": 70
    },
  ];

  constructor() { }

  ngOnInit() {
  }

  onSelect(event) {
    console.log(event);
  }

}
