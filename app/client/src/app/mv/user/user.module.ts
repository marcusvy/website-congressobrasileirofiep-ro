import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { UiModule } from '../ui/ui.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    NgxChartsModule,
    CoreModule,
    UiModule,
  ],
  declarations: [UserComponent]
})
export class UserModule { }
