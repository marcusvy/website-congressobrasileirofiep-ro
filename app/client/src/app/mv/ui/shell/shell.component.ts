import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'mv-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  @ViewChild('menu') menu;

  constructor() { }

  ngOnInit() {
  }

  onToggleMenu() {
    this.menu.onToggle();
  }
}
