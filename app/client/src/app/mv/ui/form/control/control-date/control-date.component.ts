import { Component, Renderer2, ViewChild, ElementRef, Input, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

import * as Flatpickr from 'flatpickr';
import * as FlatpickrLocalization from 'flatpickr/dist/l10n/pt';
import * as weekSelectPlugin from '../../../../core/flatpickr/plugins/week-select/week-select';
import * as confirmDatePlugin from 'flatpickr/dist/plugins/confirmDate/confirmDate';

import { AbstractControl } from './../abstract-control';

@Component({
  selector: 'mv-control-date',
  templateUrl: './control-date.component.html',
  styleUrls: ['./control-date.component.scss']
})
export class ControlDateComponent
  extends AbstractControl
  implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('formControl') formControl: ElementRef;
  @Input() value: any = '';
  @Input('id') _id: string = '';
  @Input() placeholder: string = '';
  @Input() disabled = false;
  @Input() required = false;
  errorInvalid = false;

  /**
   * Flatpickr
   */
  private flatpickr: Flatpickr;
  @Input() options: Flatpickr.Options = {};
  @Input() mode: Flatpickr.Mode = 'single';
  @Input() altFormat: string = 'd/m/Y';
  @Input() altInput: boolean = true;
  @Input() altInputClass: string = '';
  @Input() allowInput: boolean = false;
  @Input() clickOpens: boolean = true;
  @Input() dateFormat: string | null = "Y-m-d";
  @Input() defaultDate: Flatpickr.DateString | Flatpickr.DateString[];
  @Input() defaultHour: number = 12;
  @Input() defaultMinute: number = 0;
  @Input() disable: Flatpickr.DateRange[] = [];
  @Input() disableMobile: boolean = true;
  @Input() enable: Flatpickr.DateRange[] = [];
  @Input() enableTime: boolean = false;
  @Input() enableTimeOnly: boolean = false;
  @Input() enableWeek: boolean = false;
  @Input() enableSeconds: boolean = false;
  @Input() enableConfirm: boolean = false;
  @Input() hourIncrement: number = 1;
  @Input() inline: boolean = false;
  @Input() maxDate: Flatpickr.DateString = null;
  @Input() minDate: Flatpickr.DateString = null;
  @Input() minuteIncrement: number = 5;
  // @Input() nextArrow?: string;
  @Input() noCalendar: boolean = false;
  @Input() onChange: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onClose: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onOpen: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onReady: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onMonthChange: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onYearChange: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onValueUpdate: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  @Input() onDayCreate: Flatpickr.EventCallback | Flatpickr.EventCallback[];
  // @Input() prevArrow?: string;
  @Input() shorthandCurrentMonth: boolean = false;
  @Input() static: boolean = false;
  @Input() time_24hr: boolean = true;
  @Input() utc: boolean = false;
  @Input() weekNumbers?: boolean = false;

  private _plugins = [];

  // @Input() locale?: string | Locale;


  get id() {
    return this._id.concat(this._gen);
  }

  constructor(_renderer: Renderer2) {
    super(_renderer);
  }

  ngOnInit() {
    this.setupInitalValue();
  }

  ngAfterViewInit() {
    this.setupFlatpickr();
  }

  setupInitalValue() {
    this.value = this._control.value;
  }

  setupFlatpickr() {
    if (this.enableTime) {
      this.altFormat += " H:i"
    }
    if (this.enableSeconds) {
      this.altFormat += ":S";
    }
    if (this.enableTimeOnly) {
      this.altFormat = "F j, Y";
      this.altInput = false;
      this.dateFormat = "H:i";
      this.defaultHour = 12;
      this.defaultMinute = 0;
      this.enableTime = true;
      this.enableSeconds = false;
      this.noCalendar = true;
    }
    if (this.enableWeek) {
      this._plugins.push(weekSelectPlugin.weekSelectPlugin({}));
    }
    if (this.enableConfirm) {
      this._plugins.push(new confirmDatePlugin({}));
    }
    let defaultFlatpickrOptions: Flatpickr.Options = {
      locale: FlatpickrLocalization.pt,
      onChange: (selectedDates, dateStr, instance) => { this.onFlatpickrChange(selectedDates, dateStr, instance) },
      onClose: this.onClose,
      onOpen: this.onOpen,
      onReady: this.onReady,
      onMonthChange: this.onMonthChange,
      onYearChange: this.onYearChange,
      onValueUpdate: this.onValueUpdate,
      onDayCreate: this.onDayCreate,
      plugins: this._plugins
    };
    let setupOptions = Object.assign(this.options, <Flatpickr.Options>{
      mode: this.mode,
      altFormat: this.altFormat,
      altInput: this.altInput,
      altInputClass: this.altInputClass,
      allowInput: this.allowInput,
      clickOpens: this.clickOpens,
      dateFormat: this.dateFormat,
      defaultDate: this.defaultDate,
      defaultHour: this.defaultHour,
      defaultMinute: this.defaultMinute,
      disable: this.disable,
      disableMobile: this.disableMobile,
      enable: this.enable,
      enableTime: this.enableTime,
      enableSeconds: this.enableSeconds,
      hourIncrement: this.hourIncrement,
      inline: this.inline,
      maxDate: this.maxDate,
      minDate: this.minDate,
      minuteIncrement: this.minuteIncrement,

      noCalendar: this.noCalendar,

      shorthandCurrentMonth: this.shorthandCurrentMonth,
      static: this.static,
      time_24hr: this.time_24hr,
      utc: this.utc,
      weekNumbers: this.weekNumbers,
    });
    let flatpickOptions: Flatpickr.Options = Object.assign(
      defaultFlatpickrOptions,
      this.options,
      setupOptions
    );
    this.flatpickr = new Flatpickr(this.formControl.nativeElement, flatpickOptions);
  }

  /**
   * Change value of input
   *
   * @param {Date[]} selectedDates
   * @param {string} dateStr
   * @param {Flatpickr} instance
   * @memberof ControlDateComponent
   */
  onFlatpickrChange(selectedDates: Date[], dateStr: string, instance: Flatpickr) {
    let output:Date[]|string = selectedDates;
    this.value = dateStr;

    if (this.enableWeek) {
      const numberWeek = (selectedDates[0]) ? instance['config'].getWeek(selectedDates[0]) : null;
      this.value = numberWeek;
      output = numberWeek;
    }
    if(this.enableTimeOnly){
      output = dateStr;
    }
    this.value = output;
  }

  /**
   * Emit the value for control
   *
   * @param {*} event
   * @memberof ControlDateComponent
   */
  onInput(event: any) {
    let element: any = event.target;
    // let value = element.value;
    this._value.next(this.value);
    this.errorInvalid = this._control.invalid;
  }

  onFocus(result: boolean) {
    this._touch.next(result);
  }

  ngOnDestroy() {
    this.flatpickr.destroy();
  }

  onReset() {
    this.flatpickr.clear();
    this.flatpickr.redraw();
  }

}
