import { Component, OnInit, HostBinding } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'mv-control-error',
  templateUrl: './control-error.component.html',
  styleUrls: ['./control-error.component.scss']
})
export class ControlErrorComponent implements OnInit {
  
  private control:AbstractControl;

  constructor() { }

  ngOnInit() { }

  registerControl(control:AbstractControl){
    this.control = control;
  }

  @HostBinding('class.is-hidden')
  get isVisible() {
    return (this.control!==undefined) ? this.control.valid : false;
  }

}
