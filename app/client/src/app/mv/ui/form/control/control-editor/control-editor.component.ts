import { DomSanitizer } from '@angular/platform-browser';
import { ControlEditorAreaDirective } from './control-editor-area.directive';
import { Component, OnInit, Renderer2, ViewChild, ElementRef, Input, AfterViewInit, SecurityContext } from '@angular/core';
import { AbstractControl } from './../abstract-control';
import { QuillFullModules } from './quill/full-modules';
import { Quill, QuillOptionsStatic, StringMap } from 'quill';
import * as QuillFactory from 'quill';

@Component({
  selector: 'mv-control-editor',
  templateUrl: './control-editor.component.html',
  styleUrls: ['./control-editor.component.scss']
})
export class ControlEditorComponent
  extends AbstractControl
  implements OnInit, AfterViewInit {

  @Input() value = '';
  @Input() placeholder: string = '';
  @Input() disabled = false;
  @Input() required = false;
  errorInvalid = false;

  @ViewChild('controlEditorArea') quillElement: ElementRef;
  private quillEditor: Quill;
  @Input() readonly = false;
  @Input() theme: string = 'snow'; //snow, core, bubble
  @Input() modules: 'full' | StringMap = {};
  @Input() config: QuillOptionsStatic;
  @Input() output: 'text' | 'html' = 'html';
  invalidOptionsMessage;
  canInitialize = true;

  constructor(
    protected _renderer: Renderer2,
    private _sanitize: DomSanitizer
  ) {
    super(_renderer);
  }

  ngOnInit() {
    this.setupInitalValue();
    this.validateOptions();
    if (this.canInitialize) {
      this.setupQuill();
    }
  }

  private validateOptions() {
    if (this.output !== 'text', this.output !== 'html') {
      this.invalidOptionsMessage = `The output value is only: 'text' or 'html'. Fix [output='${this.output}']`;
      this.canInitialize = false;
    }
  }

  ngAfterViewInit(): void {
    if (this.canInitialize) {
      this.initQuill();
    }
  }

  setupInitalValue() {
    this.value = this._control.value;
  }

  setupQuill() {
    let quillModules = (this.modules !== 'full') ? this.modules : QuillFullModules;
    this.config = {
      modules: quillModules,
      placeholder: this.placeholder,
      readOnly: this.readonly,
      theme: this.theme,
      bounds: document.body,
    }
  }

  initQuill() {
    this.quillEditor = new QuillFactory(this.quillElement.nativeElement, Object.assign(this.config));

    // write
    if (this.value) {
      let valueSanitized = this._sanitize.sanitize(SecurityContext.HTML, this.value);
      this.quillEditor.clipboard.dangerouslyPasteHTML(valueSanitized);
    }

    this.quillEditor.on('selection-change', (range) => {
      if (!range) {
        this.onTouch(true);
      }
    });

    this.quillEditor.on('text-change', (delta, oldDelta, source) => {
      let output = '';
      switch (this.output.toLowerCase()) {
        case 'text':
          output = this.quillEditor.getText();
          break;
        case 'html':
          const html = this.quillElement.nativeElement.children[0].innerHTML;
          output = this._sanitize.sanitize(SecurityContext.HTML, html);
          if (output === '<p><br></p>') { output = ''; }
          break;
      }
      this.onInput(output);
    });
  }

  onInput(html: string) {
    let value = html;
    this._value.next(value);
    this.errorInvalid = this._control.invalid;
  }

  onFocus(result: boolean) {
    this._touch.next(result);
  }

  onTouch(result: boolean) {
    this._touch.next(result);
  }

}
