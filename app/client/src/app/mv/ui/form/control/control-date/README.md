# Control Date

## Introduction
**ControlDate** component uses [Flatpickr][1]. 

##Usage
```html
<mv-control-date mvControl>
    <mv-control-label>Date</mv-control-label>
</mv-control-date>

```

## Attributes
**@Input() value: any = '';**
Value of the control

**@Input('id') _id: string = '';**

**@Input() placeholder: string = '';**

**@Input() disabled = false;**

**@Input() required = false;**

**@Input('class') classCss = null;**

**@Input() options: Flatpickr.Options = {};**

See [List of Flatpickr Options][2]


[1]: https://chmln.github.io/flatpickr
[2]: https://chmln.github.io/flatpickr/options/
