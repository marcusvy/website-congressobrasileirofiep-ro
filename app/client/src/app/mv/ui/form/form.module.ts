import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlModule } from './control/control.module';

import { FormComponent } from './form.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FormComponent
  ],
  exports: [
    ControlModule,
    FormComponent
  ]
})
export class FormModule { }
