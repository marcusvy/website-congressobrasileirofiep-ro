import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'mv-header-filter',
  templateUrl: './header-filter.component.html',
  styleUrls: ['./header-filter.component.scss'],
  host: {
    '[style.backgroundImage]': 'getBackgroundImage()'
  }
})
export class HeaderFilterComponent implements OnInit {

  @Input() img: string = '';
  @Input() filterColor: String = '';

  constructor() { }

  ngOnInit() {

  }

  getBackgroundImage() {
    return (this.img.length > 0) ? `url('${this.img}')` : null;
  }

  getFilterStyle() {
    return (this.filterColor.length > 0) ? {
      'background-color': this.filterColor,
    } : {};
  }
}
