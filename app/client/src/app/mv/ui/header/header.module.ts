import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from '../button/button.module';
import { LayoutModule } from '../layout/layout.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { HeaderComponent } from './header.component';
import { HeaderFilterComponent } from './header-filter/header-filter.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    LayoutModule,
    ToolbarModule,
  ],
  declarations: [
    HeaderComponent,
    HeaderFilterComponent,
  ],
  exports: [
    HeaderComponent,
    HeaderFilterComponent,
  ],
})
export class HeaderModule { }
