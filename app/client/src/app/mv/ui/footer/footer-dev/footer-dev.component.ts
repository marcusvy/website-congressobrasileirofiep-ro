import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'mv-footer-dev',
  templateUrl: './footer-dev.component.html',
  styleUrls: ['./footer-dev.component.scss']
})
export class FooterDevComponent implements OnInit {

  @Input() logo = '';

  constructor() { }

  ngOnInit() {
  }

  hasLogo() {
    return this.logo.length > 0;
  }

}
