import { AvatarModule } from './../avatar/avatar.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from "../layout/layout.module";

import { FooterComponent } from './footer.component';
import { FooterAboutDirective } from './footer-about.directive';
import { FooterDevComponent } from './footer-dev/footer-dev.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    AvatarModule,
  ],
  declarations: [
    FooterComponent,
    FooterAboutDirective,
    FooterDevComponent,
  ],
  exports: [
    FooterComponent,
    FooterAboutDirective,
    FooterDevComponent,
  ],
})
export class FooterModule { }
