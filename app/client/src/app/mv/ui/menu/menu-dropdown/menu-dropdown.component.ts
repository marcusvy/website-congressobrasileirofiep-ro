import { Component, OnInit, ViewChildren, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-menu-dropdown',
  templateUrl: './menu-dropdown.component.html',
  styleUrls: ['./menu-dropdown.component.scss']
})
export class MenuDropdownComponent implements OnInit {


  private visible: Boolean = false;
  @ViewChildren('mv-menu-item') private items;

  constructor() {
  }

  ngOnInit() {

  }

  // Close on click in buttons but wait 300ms
  onBlur() {
    setTimeout(() => {
      this.close();
    }, 300);
  }

  onMouseLeaves() {
    this.close();
  }

  classMenuContainer() {
    let style = {
      'js-mv-menu--visible': this.visible
    };
    return style;
  }

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }

  toggle(): Boolean {
    this.visible = !this.visible;
    return this.visible;
  }

}
