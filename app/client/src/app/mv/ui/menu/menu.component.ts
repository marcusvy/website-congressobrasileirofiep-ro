import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-menu',
  template: '',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuComponent {

  constructor() {
  }
}
