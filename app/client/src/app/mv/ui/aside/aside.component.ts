import { Component, OnInit, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'mv-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  @Input() title = '';
  @HostBinding('class.is-fixed')
  @Input() fixed = true;
  @HostBinding('class.is-opened')
  opened = false;

  constructor() { }

  ngOnInit() {}

  toggle() {
    this.opened = !this.opened;
  }
  open() {
    this.opened = true;
  }
  close() {
    this.opened = false;
  }

}
