import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutModule } from '../layout/layout.module';
import { IconModule } from '../icon/icon.module';
import { ButtonModule } from '../button/button.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { AsideComponent } from './aside.component';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    IconModule,
    ButtonModule,
    ToolbarModule,
  ],
  declarations: [
    AsideComponent
  ],
  exports: [
    AsideComponent
  ]
})
export class AsideModule { }
