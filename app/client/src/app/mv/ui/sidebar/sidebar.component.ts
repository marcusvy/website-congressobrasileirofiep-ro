import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SidebarComponent implements OnInit {

  private open:boolean = false;
  @Input() title:string;

  constructor() { }

  ngOnInit() {
  }

  toggle () {
    this.open = !this.open;
  }
}
