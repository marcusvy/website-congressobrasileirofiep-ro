import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconModule } from '../icon/icon.module';
import { LayoutModule } from '../layout/layout.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { SidebarComponent } from './sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    LayoutModule,
    ToolbarModule,
  ],
  declarations: [
    SidebarComponent,
  ],
  exports: [
    SidebarComponent,
  ]
})
export class SidebarModule {
}
