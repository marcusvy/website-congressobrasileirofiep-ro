import { Component, OnInit, Input, HostBinding } from '@angular/core';

import { MV_ANIMATION_TAB } from './../../core/animation/tab.animation';

@Component({
  selector: 'mv-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
  animations: [ MV_ANIMATION_TAB ]
})
export class TabComponent implements OnInit {

  @Input() icon: string;
  @Input() label: string = '';
  @Input() active: boolean = false;

  @HostBinding('@tab')
  get tabAnimation() {
    return (this.active) ? 'active' : 'inactive';
  }

  constructor() { }

  ngOnInit() {
  }

}
