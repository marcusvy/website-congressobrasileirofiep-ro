import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from './../button/button.module';
import { IconModule } from './../icon/icon.module';
import { LayoutModule } from './../layout/layout.module';

import { TabComponent } from './tab.component';
import { TabGroupComponent } from './tab-group/tab-group.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    LayoutModule,
    ButtonModule,
  ],
  declarations: [
    TabComponent,
    TabGroupComponent,
  ],
  exports: [
    TabComponent,
    TabGroupComponent,
  ]
})
export class TabModule { }
