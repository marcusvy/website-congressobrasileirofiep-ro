import { Component, AfterContentInit, ContentChildren, QueryList } from '@angular/core';
import { TabComponent } from './../tab.component';

@Component({
  selector: 'mv-tab-group',
  templateUrl: './tab-group.component.html',
  styleUrls: ['./tab-group.component.scss']
})
export class TabGroupComponent implements AfterContentInit {

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  constructor() { }

  ngAfterContentInit(): void {
    let activeTabs = this.tabs.filter((tab) => tab.active);
    if (activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab: TabComponent) {
    this.tabs.map(tab => tab.active = false);
    tab.active = true;
  }

}
