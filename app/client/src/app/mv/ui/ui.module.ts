import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * Ui Modules
 */
import { AsideModule } from './aside/aside.module';
import { AvatarModule } from './avatar/avatar.module';
import { BadgeModule } from './badge/badge.module';
import { ButtonModule } from './button/button.module';
import { CardModule } from './card/card.module';
import { DataModule } from './data/data.module';
import { FooterModule } from './footer/footer.module';
import { FormModule } from './form/form.module';
import { HeaderModule } from './header/header.module';
import { IconModule } from './icon/icon.module';
import { LayoutModule } from './layout/layout.module';
import { ListModule } from './list/list.module';
import { LoadbarModule } from './loadbar/loadbar.module';
import { MenuModule } from './menu/menu.module';
import { ModalModule } from './modal/modal.module';
import { ModelModule } from './model/model.module';
import { PaperModule } from './paper/paper.module';
import { ScrollerModule } from './scroller/scroller.module';
import { SectionModule } from './section/section.module';
import { ShellModule } from './shell/shell.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { TabModule } from './tab/tab.module';
import { ToolbarModule } from './toolbar/toolbar.module';

/**
 * Ui component
 */
import { UiComponent } from './ui.component';

const listUiModules = [
  ButtonModule,
  IconModule,
  LayoutModule,
  AsideModule,
  AvatarModule,
  BadgeModule,
  CardModule,
  DataModule,
  FooterModule,
  FormModule,
  HeaderModule,
  ListModule,
  LoadbarModule,
  MenuModule,
  ModalModule,
  ModelModule,
  PaperModule,
  ScrollerModule,
  SectionModule,
  ShellModule,
  SidebarModule,
  TabModule,
  ToolbarModule,
];


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    // FormsModule,
    ReactiveFormsModule,
    ... listUiModules
  ],
  declarations: [
    UiComponent,
  ],
  exports: [
    UiComponent,
    ... listUiModules
  ]
})
export class UiModule { }
