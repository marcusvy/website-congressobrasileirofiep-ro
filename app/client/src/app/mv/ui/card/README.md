# Card

```html
<mv-card class="has-bg" img="/assets/img/background/onca-pintada.jpg">
  <mv-card-title>
    <h1 class="is-title">Um exemplo</h1>
    <h2 class="is-subtitle">Um subtítulo especial</h2>
  </mv-card-title>
  <mv-card-text>
    <p>
      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, optio eaque ratione quos impedit ab tenetur enim minima vel
      aliquam rerum reiciendis repellendus, quasi in? Aliquid quos amet explicabo fuga.
    </p>
  </mv-card-text>
  <mv-card-body>
    <mv-list class="remove-padding">
      <mv-list-item>
        <h1>Header List</h1>
        <p>Lorem ipsum dolor sit amet consectetur</p>
      </mv-list-item>
      <mv-list-item>
        <h1>Header List</h1>
        <p>Lorem ipsum dolor sit amet consectetur</p>
      </mv-list-item>
      <mv-list-item>
        <h1>Header List</h1>
        <p>Lorem ipsum dolor sit amet consectetur</p>
      </mv-list-item>
    </mv-list>
  </mv-card-body>
  <mv-card-action>
    <button class="mv-btn">
      <mv-icon name="user"></mv-icon>
      <span>Um usuário</span>
    </button>
  </mv-card-action>
</mv-card>


```

## Modificadores

  - .remove-padding
  - .has-bg  
