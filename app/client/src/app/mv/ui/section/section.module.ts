import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutModule } from './../layout/layout.module';

import { SectionComponent } from './section.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
  ],
  declarations: [
    SectionComponent,
  ],
  exports: [
    SectionComponent
  ]
})
export class SectionModule { }
