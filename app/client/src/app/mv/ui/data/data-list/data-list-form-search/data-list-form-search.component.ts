import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'mv-data-list-form-search',
  templateUrl: './data-list-form-search.component.html',
  styleUrls: ['./data-list-form-search.component.scss']
})
export class DataListFormSearchComponent implements OnInit {

  form: FormGroup;
  @Output() onInit = new EventEmitter();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.buildFormSearch();
  }

  buildFormSearch(){
    this.form = this.formBuilder.group({
      'for': ['', Validators.compose([
        Validators.required
      ])],
      'in': ['description', Validators.compose([
        Validators.required
      ])],
    });
    this.onInit.emit(this.form);
  }

}
