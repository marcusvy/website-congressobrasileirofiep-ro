import {
  Component,
  OnInit,
  OnDestroy,
  AfterContentInit,
  EventEmitter,
  Output,
  Input,
  ViewChild,
  ViewChildren,
  QueryList
} from '@angular/core';
import { DataListModalSearchComponent } from "./data-list-modal-search/data-list-modal-search.component";
import { DataListModalDeleteComponent } from "./data-list-modal-delete/data-list-modal-delete.component";
import { DataListModalFormComponent } from "./data-list-modal-form/data-list-modal-form.component";

import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'mv-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit, OnDestroy {

  private entitySubscription: Subscription;
  @Input() entity;

  @Input() collection: any[] = [];
  @Input() formSearch: any;
  @Input() modeForm: boolean = false;
  @Input() modeDelete: boolean = false;
  @Input() modeEdit: boolean = false; //trocar por update
  @Input() modeSearch: boolean = false;

  @Output() onModeForm = new EventEmitter();
  @Output() onModeDelete = new EventEmitter();
  @Output() onModeEdit = new EventEmitter();
  @Output() onModeSearch = new EventEmitter();

  @Output() onCrudCreate = new EventEmitter(); //goToForm Create
  @Output() onCrudUpdate = new EventEmitter(); //goToForm Create
  @Output() onCrudSearch = new EventEmitter(); //openModal Search
  @Output() onCrudDelete = new EventEmitter(); //openModal Delete
  @ViewChild(DataListModalDeleteComponent) private deleteModal;
  @ViewChild(DataListModalSearchComponent) private searchModal;
  @ViewChild(DataListModalFormComponent) private formModal;


  constructor() {
  }

  ngOnInit() {
    this.entitySubscription = this.entity.subscribe((entity) => {this.entity = entity;});
  }

  ngOnDestroy() {
    this.entitySubscription.unsubscribe();
  }

  isCollectionEmpty() {
    return (this.collection.length === 0);
  }

  onDisableModeDelete() {
    this.modeDelete = false;
    this.onModeDelete.emit(this.modeDelete);
  }

  onEnableModeDelete() {
    this.modeDelete = true;
    this.onModeDelete.emit(this.modeDelete);
  }

  onOpenModalDelete(entity) {
    // this.entity.next(entity);
    this.deleteModal.open();
  }

  onDisableModeSearch() {
    this.modeSearch = false;
    this.onModeSearch.emit(this.modeSearch);
  }

  onEnableModeSearch() {
    this.modeSearch = true;
    this.onModeSearch.emit(this.modeSearch);
  }

  onOpenModalSearch() {
    this.searchModal.open();
  }

  onSearch($event) {
    this.onCrudSearch.emit($event);
    this.onEnableModeSearch();
  }

  onDisableModeForm() {
    this.modeForm = false;
    this.onModeForm.emit(this.modeForm);
  }

  onEnableModeForm() {
    this.modeForm = true;
    this.onModeForm.emit(this.modeForm);
  }

  onOpenModalForm() {
    this.formModal.open();
  }

  onCloseModalForm() {
    this.formModal.close();
  }

  onCreate($event) {
    this.onCrudCreate.emit($event);
  }

  onUpdate($event) {
    this.onCrudUpdate.emit($event);
  }

  onDelete($event) {
    this.onCrudDelete.emit($event);
  }

  onClean($event) {
    this.modeSearch = $event;
  }
}
