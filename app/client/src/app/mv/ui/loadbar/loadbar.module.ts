import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadbarComponent } from './loadbar.component';
import { LoadbarService } from './loadbar.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoadbarComponent
  ],
  exports: [
    LoadbarComponent
  ],
  providers: [
    LoadbarService
  ]
})
export class LoadbarModule { }
