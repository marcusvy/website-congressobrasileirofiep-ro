import { ModelMode } from './model/ModelMode';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ModelService {

  private _mode: BehaviorSubject<ModelMode> = new BehaviorSubject(new ModelMode());

  constructor() { }

  get modeChanges(): Observable<ModelMode> {
    return this._mode.asObservable();
  }

  setMode(value:ModelMode) {
    this._mode.next(value);
  }
}
