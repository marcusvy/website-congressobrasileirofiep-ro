export class ModelMode {
  deleteConfirm: boolean = false;
  delete: boolean = false;
  edit: boolean = false;
  form: boolean = false;
  list: boolean = false;
  search: boolean = false;

  constructor() { }

  enableDelete() {
    this.delete = true;
  }

  disableDelete() {
    this.delete = false;
  }

  enableDeleteConfirm() {
    this.deleteConfirm = true;
  }

  disableDeleteConfirm() {
    this.deleteConfirm = false;
  }

  enableEdit() {
    this.edit = true;
  }

  disableEdit() {
    this.edit = false;
  }

  enableSearch() {
    this.search = true;
  }

  disableSearch() {
    this.search = false;
  }
  enableList() {
    this.list = true;
  }

  disableList() {
    this.list = false;
  }
  reset() {
    this.delete = false;
    this.deleteConfirm = false;
    this.edit = false;
    this.form = false;
    this.list = false;
    this.search = false;
  }
}
