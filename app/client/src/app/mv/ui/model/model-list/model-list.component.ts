import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { SearchParam } from './../../../core/http/search/search-param';
import { ModalComponent } from './../../modal/modal.component';
import { LoadbarService } from './../../loadbar/loadbar.service';

import { ModelService } from './../model.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'mv-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.scss']
})
export class ModelListComponent implements OnInit {

  fetchSubscription: Subscription;
  searchSubscription: Subscription;
  aprovedSubscription: Subscription;
  @Input() isDeleteModeEnabled = false;
  @Input('isSearchModeEnabled') _isSearchModeEnabled = false;
  @Input() isAprovedModeEnabled = false;
  @ViewChild('modalDelete') modalDelete: ModalComponent;
  @ViewChild('modalAlert') modalAlert: ModalComponent;
  private _collection: BehaviorSubject<any[]>;
  private _storage: {
    collection: any[],
    selected: any | null
  };

  constructor(private _service: ModelService, private _loadbar:LoadbarService) {
    this._collection = new BehaviorSubject<any[]>([]);
    this._storage = { collection: [], selected: null };
  }

  ngOnInit() {
  }

  get total(): number {
    return this._storage.collection.length;
  }

  get collection(): Observable<any[]> {
    return this._collection.asObservable();
  }

  get selected(): any {
    return this._storage.selected;
  }

  get isSearchModeEnabled() {
    return this._isSearchModeEnabled;
  }
  set isSearchModeEnabled($value) {
    this._isSearchModeEnabled = $value;

    if (!$value) {
      this.onFetchAll();
    }
  }

  toggleDeleteMode() {
    this.isDeleteModeEnabled = !this.isDeleteModeEnabled;
  }
  toggleSearchMode() {
    this.isSearchModeEnabled = !this.isSearchModeEnabled;
  }
  toggleAprovedMode() {
    this.isAprovedModeEnabled = !this.isAprovedModeEnabled;
  }

  onFetchAll() {
    this._loadbar.setStatus(true)

    // this.service.fetchAll()
    //   .map(data => data.collection)
    //   .subscribe(collection => {
    //     this._storage.collection = collection;
    //     return this._collection.next(Object.assign({}, this._storage).collection);
    //   }, error => console.log('não foi possível carregar lista')
    //   , () => this.loading = false);
  }

  onSearch(collection: any[]) {
    this._storage.collection = collection;
    this._collection.next(Object.assign({}, this._storage).collection);
  }

  onAproved(entity: any) {
    let status = !entity.is_active;
    this._loadbar.setStatus(true);
    // this.aprovedSubscription = this.service.activate(entity.id, +status)
    //   .filter((params) => params.id !== undefined)
    //   .mergeMap((params) => this.service.fetch(params.id))
    //   .map(response => response.item)
    //   .subscribe(modal => {
    //     this._storage.collection = this._storage.collection.map(item => (item.id == modal.id) ? modal : item);
    //     this._collection.next(Object.assign({}, this._storage).collection);
    //   }, (error) => { }, () => this.loading = false);
  }


  /**
   * Abre confirmação de remoção
   * @param entity any
   */
  onDeleteItem(entity: any) {
    this._storage.selected = entity;
    this.modalDelete.open();
  }

  /**
   * Após confirmado, remove o item
   */
  onDeleteConfirmation() {
    this._loadbar.setStatus(true);
    // this.service.delete(this._storage.selected.id)
    //   .subscribe(data => {
    //     if (data.success) {
    //       this.onDeleteComplete();
    //     }
    //   }, (error: any) => { console.log(error); }
    //   , () => this.loading = false);
  }

  /**
   * Após banco de dados remove da lista
   */
  onDeleteComplete() {
    this._storage.collection.splice(this._storage.collection.indexOf(this._storage.selected), 1);
    this._collection.next(Object.assign({}, this._storage).collection);
    this.modalDelete.close();
    this._storage.selected = null;
  }


}
