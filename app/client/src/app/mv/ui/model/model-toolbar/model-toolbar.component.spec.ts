import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelToolbarComponent } from './model-toolbar.component';

describe('ModelToolbarComponent', () => {
  let component: ModelToolbarComponent;
  let fixture: ComponentFixture<ModelToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
