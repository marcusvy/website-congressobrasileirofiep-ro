import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from './../button/button.module';
import { IconModule } from './../icon/icon.module';
import { LayoutModule } from './../layout/layout.module';
import { ListModule } from './../list/list.module';
import { LoadbarModule } from './../loadbar/loadbar.module';
import { ModalModule } from './../modal/modal.module';
import { TabModule } from './../tab/tab.module';
import { ToolbarModule } from './../toolbar/toolbar.module';

import { ModelComponent } from './model.component';
import { ModelListComponent } from './model-list/model-list.component';
import { ModelFormComponent } from './model-form/model-form.component';
import { ModelItemDirective } from './directives/model-item.directive';
import { ModelToolbarComponent } from './model-toolbar/model-toolbar.component';
import { ModelDashboardDirective } from './directives/model-dashboard.directive';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    LayoutModule,
    ListModule,
    LoadbarModule,
    ModalModule,
    TabModule,
    ToolbarModule,
  ],
  declarations: [
    ModelComponent,
    ModelListComponent,
    ModelFormComponent,
    ModelItemDirective,
    ModelToolbarComponent,
    ModelDashboardDirective,
  ],
  exports:[
    ModelComponent,
    ModelListComponent,
    ModelFormComponent,
    ModelItemDirective,
    ModelToolbarComponent,
    ModelDashboardDirective,
  ]
})
export class ModelModule { }
