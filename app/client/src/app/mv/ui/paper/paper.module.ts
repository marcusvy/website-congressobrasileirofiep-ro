import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageDirective } from './page.directive';
import { PaperComponent } from './paper.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PageDirective,
    PaperComponent
  ],
  exports: [
    PageDirective,
    PaperComponent
  ]
})
export class PaperModule { }
