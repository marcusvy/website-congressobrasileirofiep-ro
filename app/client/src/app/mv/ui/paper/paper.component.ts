import { PaperSizes } from './paper-sizes';
import { Component, OnInit, ViewEncapsulation, Input, Inject, Renderer } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'mv-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PaperComponent implements OnInit {

  @Input() size: string | null = null;
  @Input() orientation: string | null = null;
  @Input() padding: string | null = null;

  constructor(
    @Inject(DOCUMENT) private document: HTMLDocument,
    private render: Renderer
  ) {
  }

  ngOnInit() {
    const body = this.document.body;
    this.render.setElementClass(body, 'mv-paper', true);
    if (this.size !== null) {
      this.render.setElementClass(body, this.getPaperSize(), true);
    }
    if (this.orientation !== null) {
      this.render.setElementClass(body, this.getPaperOrientation(), true);
    }
    if (this.padding !== null) {
      this.render.setElementClass(body, this.getPaperPadding(), true);
    }
  }

  private getPaperSize() {
    return this.size.toUpperCase();
  }

  private getPaperOrientation() {
    return this.orientation.toLowerCase();
  }

  private getPaperPadding() {
    return 'mv-page-padding-' + this.padding.toLowerCase();
  }
}
