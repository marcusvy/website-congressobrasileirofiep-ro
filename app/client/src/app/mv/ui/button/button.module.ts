import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonComponent } from './button.component';
import { ButtonTopComponent } from './button-top/button-top.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ButtonComponent,
    ButtonTopComponent,
  ],
  exports: [
    ButtonComponent,
    ButtonTopComponent,
  ],
})
export class ButtonModule { }
