import { Injectable } from '@angular/core';

@Injectable()
export class MinicursoService {

  constructor() { }

  fetchAll() {
    return [
      {
        id: 1,
        name: 'Mito-drama: Teatro e Narrativas de autoria indígena',
        professor: 'José Maria Lopes Júnior (UNIR)'
      },
      {
        id: 2,
        name: 'Poéticas Amazônicas: a literatura como expressão da cultura amazonense',
        professor: 'Gabriel Pereira de Melo (UEA)'
      },
      {
        id: 3,
        name: 'Ecomuseu em pauta',
        professor: 'Terezinha Rezende (Ecomuseu da Amazônia)'
      },
      {
        id: 4,
        name: 'História e cinema',
        professor: 'Veronica Aparecida Silveira Aguiar (UNIR)'
      },
      {
        id: 5,
        name: 'Filosofia da literatura como método fronteiriço',
        professor: 'Vitor Cei Santos (UNIR)'
      },
      {
        id: 6,
        name: 'Antropología histórica de la escritura en el siglo XVI',
        professor: 'Miguel Ángel Segundo Guzmán (Mexico)'
      },
      {
        id: 7,
        name: 'Interlocuções entre Literatura Latino-Americana, Filosofia e Direito: uma leitura transdisciplinar das fá Negra" e "A Falácia do Mal Menor" de Augusto Monterroso',
        professor: 'Marcus Vinícius Xavier de Oliveira'
      },
      {
        id: 8,
        name: 'Colonialismo, sexualidade e gênero na América Latina',
        professor: 'Estevão Rafael Fernandes e Maíra Silva Ribeiro'
      },
    ]
  }

}
