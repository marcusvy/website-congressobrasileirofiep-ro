import { TestBed, inject } from '@angular/core/testing';

import { MinicursoService } from './minicurso.service';

describe('MinicursoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MinicursoService]
    });
  });

  it('should be created', inject([MinicursoService], (service: MinicursoService) => {
    expect(service).toBeTruthy();
  }));
});
