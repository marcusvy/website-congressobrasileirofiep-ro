import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MinicursoService } from './minicurso.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    MinicursoService,
  ]
})
export class MinicursoModule { }
