
import { AuthGuard } from './../auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CongressoAdminComponent } from './congresso-admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InscricaoComponent } from './inscricao/inscricao.component';
import { SimposioComponent } from './simposio/simposio.component';
import { SimposioDetailComponent } from './simposio/simposio-detail/simposio-detail.component';
import { SimposioListComponent } from './simposio/simposio-list/simposio-list.component';
import { SimposioFormComponent } from './simposio/simposio-form/simposio-form.component';
import { SimposioReportComponent } from './simposio/simposio-report/simposio-report.component';

// import { CongressoComponent } from './congresso.component';
// import { NormasComponent } from './normas/normas.component';
// import { SimposioComponent } from './simposio/simposio.component';
// import { ComunicacaoComponent } from './comunicacao/comunicacao.component';
// import { MinicursoComponent } from './minicurso/minicurso.component';
// import { OuvinteComponent } from './ouvinte/ouvinte.component';
// import { OrientacaoComponent } from './orientacao/orientacao.component';

const routes: Routes = [{
  path: '',
  component: CongressoAdminComponent,
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    { path: '', component: DashboardComponent, pathMatch: 'full' },
    { path: 'inscricao', component: InscricaoComponent },
    // { path: 'normas', component: NormasComponent },
    // { path: 'eixos', component: EixosComponent },
    {
      path: 'simposio', component: SimposioComponent, children: [
        { path: '', redirectTo: 'list', pathMatch: 'full' },
        { path: 'list', component: SimposioListComponent },
        { path: 'form', component: SimposioFormComponent },
        { path: 'form/:id', component: SimposioFormComponent },
        { path: 'detail/:id', component: SimposioDetailComponent },
        { path: 'report', component: SimposioReportComponent }
      ]
    },
    // { path: 'comunicacao', component: ComunicacaoComponent },
    // { path: 'minicurso', component: MinicursoComponent },
    // { path: 'ouvinte', component: OuvinteComponent },
    // { path: 'orientacao', component: OrientacaoComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CongressoAdminRoutingModule { }
