import { EixoModule } from './eixo/eixo.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CongressoAdminSharedModule } from './shared/shared.module';
import { CongressoAdminRoutingModule } from './congresso-admin-routing.module';

import { CongressoAdminComponent } from './congresso-admin.component';
import { SimposioComponent } from './simposio/simposio.component';
import { SimposioReportComponent } from './simposio/simposio-report/simposio-report.component';

import { SimposioListComponent } from './simposio/simposio-list/simposio-list.component';
import { SimposioItemComponent } from './simposio/simposio-item/simposio-item.component';
import { SimposioFormComponent } from './simposio/simposio-form/simposio-form.component';
import { SimposioDetailComponent } from './simposio/simposio-detail/simposio-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InscricaoComponent } from './inscricao/inscricao.component';
import { SimposioFormSearchComponent } from './simposio/simposio-form-search/simposio-form-search.component';

import { ProponenteService } from './proponente/proponente.service';

@NgModule({
  imports: [
    CommonModule,
    CongressoAdminSharedModule,
    CongressoAdminRoutingModule,
  ],
  declarations: [
    CongressoAdminComponent,
    SimposioComponent,
    SimposioReportComponent,
    SimposioListComponent,
    SimposioItemComponent,
    SimposioFormComponent,
    SimposioFormSearchComponent,
    SimposioDetailComponent,
    DashboardComponent,
    InscricaoComponent,
  ],
  exports:[
    EixoModule
  ],
  providers: [
    ProponenteService
  ]
})
export class CongressoAdminModule { }
