import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'mv-congresso-admin',
  templateUrl: './congresso-admin.component.html',
  styleUrls: ['./congresso-admin.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CongressoAdminComponent implements OnInit {

  open = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.onRouterEvents();
  }

  onRouterEvents() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((event) => {
        this.open = false;
        window.scrollTo(0,0);
      });
  }

  toggleMenu() {
    this.open = !this.open;
  }

}
