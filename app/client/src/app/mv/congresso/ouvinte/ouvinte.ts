export interface Ouvinte {
  nome?: string;
  situacao?:string;
  instituicao_sigla?:string;
  instituicao_nome?:string;
  email?:string;
  telefone_fixo?:string;
  telefone_celular?:string;
}
