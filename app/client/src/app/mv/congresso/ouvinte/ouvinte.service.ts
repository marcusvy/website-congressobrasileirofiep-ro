import { Injectable, Inject } from '@angular/core';
import { Response, Http } from '@angular/http';
import { ApiHttpService } from '../../core/http/api-http.service';
import { ConnectionService } from "../../core/connection.service";

@Injectable()
export class OuvinteService
  extends ApiHttpService {

  protected token: string = 'congresso::ouvinte';
  protected url: string = '/congresso-ouvinte';

}
