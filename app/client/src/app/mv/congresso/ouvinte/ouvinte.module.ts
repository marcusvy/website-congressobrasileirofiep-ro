import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OuvinteService } from './ouvinte.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    OuvinteService,
  ]
})
export class OuvinteModule { }
