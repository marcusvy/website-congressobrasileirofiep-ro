import { TestBed, inject } from '@angular/core/testing';

import { OuvinteService } from './ouvinte.service';

describe('OuvinteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OuvinteService]
    });
  });

  it('should be created', inject([OuvinteService], (service: OuvinteService) => {
    expect(service).toBeTruthy();
  }));
});
