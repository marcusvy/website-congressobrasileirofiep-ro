import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProponenteService } from './proponente.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers:[
    ProponenteService
  ]
})
export class ProponenteModule { }
