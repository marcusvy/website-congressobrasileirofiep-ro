import { TestBed, inject } from '@angular/core/testing';

import { ProponenteService } from './proponente.service';

describe('ProponenteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProponenteService]
    });
  });

  it('should be created', inject([ProponenteService], (service: ProponenteService) => {
    expect(service).toBeTruthy();
  }));
});
