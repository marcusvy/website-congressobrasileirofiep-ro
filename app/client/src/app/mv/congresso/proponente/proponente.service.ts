import { Injectable } from '@angular/core';

@Injectable()
export class ProponenteService {

  constructor() { }

  fetchTitulos() {
    return [
      { id: 1, name: 'Graduado' },
      { id: 2, name: 'Especialista' },
      { id: 3, name: 'Mestre' },
      { id: 4, name: 'Doutor' },
      { id: 5, name: 'Pós-Doutor' },
      { id: 6, name: 'Livre Docente' },
    ];
  }

  fetchVinculos() {
    return [
      { id: 1, name: 'Pesquisador' },
      { id: 2, name: 'Professor' },
      { id: 3, name: 'Pós-Doutorando' },
      { id: 4, name: 'Doutorando' },
      { id: 5, name: 'Mestrando' },
      { id: 6, name: 'Especializando' },
      { id: 7, name: 'Graduando' },
    ];
  }

}
