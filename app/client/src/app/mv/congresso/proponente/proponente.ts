export interface Proponente {
  nome?:string;
  email?:string;
  titulacao?:number;
  vinculo?:number;
  instituicao_sigla?:string;
  instituicao_nome?:string;
}
