import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './../../core/core.module';
import { UiModule } from '../../ui/ui.module';

import { CongressoAdminRoutingModule } from './../congresso-admin-routing.module';
import { CongressoAdminMenuComponent } from './congresso-admin-menu/congresso-admin-menu.component';

import { TitulacaoPipe } from './pipe/titulacao.pipe';
import { VinculoPipe } from './pipe/vinculo.pipe';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    UiModule,
    CongressoAdminRoutingModule,
  ],
  declarations: [
    CongressoAdminMenuComponent,
    TitulacaoPipe,
    VinculoPipe,
  ],
  exports: [
    CoreModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule,
    CongressoAdminMenuComponent,
    TitulacaoPipe,
    VinculoPipe,
  ]
})
export class CongressoAdminSharedModule { }
