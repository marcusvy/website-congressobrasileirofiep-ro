import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mvSimposioVinculo'
})
export class VinculoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const v = Number.parseInt(value);
    switch(v) {
    case 1 : return 'Pesquisador';
    case 2 : return 'Professor';
    case 3 : return 'Pós-Doutorando';
    case 4 : return 'Doutorando';
    case 5 : return 'Mestrando';
    case 6 : return 'Especializando';
    case 7 : return 'Graduando';
    default: return 'Vinculo desconhecido';
    }
  }

}
