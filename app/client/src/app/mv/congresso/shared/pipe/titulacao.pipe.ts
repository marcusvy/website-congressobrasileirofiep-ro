import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mvSimposioTitulacao'
})
export class TitulacaoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const v = Number.parseInt(value);

    switch(v){
     case 1: return 'Graduado';
     case 2: return 'Especialista';
     case 3: return 'Mestre';
     case 4: return 'Doutor';
     case 5: return 'Pós-Doutor';
     case 6: return 'Livre Docente';
     default: return 'Titulação desconhecida';
    }
  }

}
