import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongressoAdminMenuComponent } from './congresso-admin-menu.component';

describe('CongressoAdminMenuComponent', () => {
  let component: CongressoAdminMenuComponent;
  let fixture: ComponentFixture<CongressoAdminMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongressoAdminMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongressoAdminMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
