import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-congresso-admin-menu',
  templateUrl: './congresso-admin-menu.component.html',
  styleUrls: ['./congresso-admin-menu.component.scss']
})
export class CongressoAdminMenuComponent implements OnInit {


  menu = [
    {
      label: "Inscrição", route: '/congresso/inscricao', children: [
        { label: "Simpósio", route: '/congresso/simposio' },
        // { label: "Comunicação", route: '/congresso/comunicacao' },
        // { label: "Mini-Cursos", route: '/congresso/minicurso' },
        // { label: "Ouvinte", route: '/congresso/ouvinte' },
        // { label: "Submissão", route: '/congresso/submissao' }, @todo remove
      ]
    },
    {
      label: "Congresso", route: '/congresso/simposio', children: [
        { label: "Programação", route: '/congresso/simposio' },
      ]
    },
    {label: "Palestrantes", route: '/congresso/simposio'},
    {label: "Curso", route: '/congresso/simposio'},
    {label: "Trabalhos Científicos", route: '/congresso/simposio'},
    {label: "Contato", route: '/congresso/simposio'},
    {
      label: "Galeria", route: '/congresso/simposio', children: [
        { label: "Fotos", route: '/congresso/simposio' },
        { label: "Videos", route: '/congresso/simposio' },
      ]
    },


  ];
  constructor() { }

  ngOnInit() {
  }

}
