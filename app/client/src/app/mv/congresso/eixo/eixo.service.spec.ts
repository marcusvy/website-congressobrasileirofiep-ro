import { TestBed, inject } from '@angular/core/testing';

import { EixoService } from './eixo.service';

describe('EixoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EixoService]
    });
  });

  it('should be created', inject([EixoService], (service: EixoService) => {
    expect(service).toBeTruthy();
  }));
});
