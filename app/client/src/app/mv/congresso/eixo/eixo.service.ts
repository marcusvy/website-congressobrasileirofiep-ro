import { Injectable, Inject } from '@angular/core';
import { Response, Http } from '@angular/http';
import { ApiHttpService } from '../../core/http/api-http.service';
import { ConnectionService } from "../../core/connection.service";

@Injectable()
export class EixoService
  extends ApiHttpService {

  protected token: string = 'congresso::eixo';
  protected url: string = '/congresso-eixo';
}
