import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CongressoAdminSharedModule } from './../shared/shared.module';

import { EixoService } from './eixo.service';

@NgModule({
  imports: [
    CommonModule,
    CongressoAdminSharedModule,
  ],
  declarations: [
  ],
  exports:[
  ],
  providers: [
    EixoService,
  ]
})
export class EixoModule { }
