import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SearchParam } from './../../../core/http/search/search-param';

import { SimposioService } from './../simposio.service';
import { Simposio } from './../simposio';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'mv-simposio-form-search',
  templateUrl: './simposio-form-search.component.html',
  styleUrls: ['./simposio-form-search.component.scss']
})
export class SimposioFormSearchComponent implements OnInit, OnDestroy {

  @Input('modeEnabled') _modeEnabled = false;
  @Output() onSearch: EventEmitter<Simposio[]> = new EventEmitter();
  form: FormGroup;
  private searchSubscription: Subscription;
  private param: SearchParam;

  constructor(
    private fb: FormBuilder,
    private service: SimposioService
  ) {
    this.addControls();
  }

  addControls() {
    this.form = this.fb.group({
      search: ['']
    });
  }

  get modeEnabled() {
    return this._modeEnabled;
  }

  set modeEnabled(value) {
    this._modeEnabled = value;
  }

  ngOnInit() {
    this.searchSubscription = this.form.controls.search.valueChanges
      .debounceTime(1000)
      .switchMap((text) => this.service.search({ in: 'titulo', for: text }))
      .map(response => response.collection)
      .subscribe(collection => this.onSearch.emit(collection));
  }

  ngOnDestroy() {
    this.form.reset();
    this.onSearch.unsubscribe();
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

}

