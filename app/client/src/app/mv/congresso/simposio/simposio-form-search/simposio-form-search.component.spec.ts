import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioFormSearchComponent } from './simposio-form-search.component';

describe('SimposioFormSearchComponent', () => {
  let component: SimposioFormSearchComponent;
  let fixture: ComponentFixture<SimposioFormSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioFormSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioFormSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
