import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioListComponent } from './simposio-list.component';

describe('SimposioListComponent', () => {
  let component: SimposioListComponent;
  let fixture: ComponentFixture<SimposioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
