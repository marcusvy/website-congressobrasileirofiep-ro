import { Component, OnInit, Input, ViewChild, Output, EventEmitter, HostBinding, OnDestroy } from '@angular/core';
import { ModalComponent } from './../../../ui/modal/modal.component';
import { SearchParam } from './../../../core/http/search/search-param';

import { SimposioService } from './../simposio.service';
import { Simposio } from './../simposio';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';


@Component({
  selector: 'mv-simposio-list',
  templateUrl: './simposio-list.component.html',
  styleUrls: ['./simposio-list.component.scss']
})
export class SimposioListComponent implements OnInit, OnDestroy {

  fetchSubscription: Subscription;
  searchSubscription:Subscription;
  aprovedSubscription: Subscription;
  @Input() isDeleteModeEnabled = false;
  @Input('isSearchModeEnabled') _isSearchModeEnabled = false;
  @Input() isAprovedModeEnabled = false;
  @ViewChild('modalDelete') modalDelete: ModalComponent;
  @ViewChild('modalAlert') modalAlert: ModalComponent;
  loading: boolean;
  private _collection: BehaviorSubject<Simposio[]> = new BehaviorSubject<Simposio[]>([]);
  private storage: {
    collection: Simposio[],
    selected: Simposio | null
  };

  constructor(private service: SimposioService) {
    this._collection = new BehaviorSubject<Simposio[]>([]);
    this.storage = { collection: [], selected: null };
  }

  ngOnInit() {
    this.onFetchAll();
  }

  ngOnDestroy() {
    if (this.fetchSubscription) {
      this.fetchSubscription.unsubscribe();
    }
    if (this.aprovedSubscription) {
      this.aprovedSubscription.unsubscribe();
    }
    if(this.searchSubscription){
      this.searchSubscription.unsubscribe();
    }
  }

  get total():number {
    return this.storage.collection.length;
  }

  get collection(): Observable<Simposio[]> {
    return this._collection.asObservable();
  }

  get selected(): Simposio {
    return this.storage.selected;
  }

  get isSearchModeEnabled() {
    return this._isSearchModeEnabled;
  }
  set isSearchModeEnabled($value) {
    this._isSearchModeEnabled = $value;

    if(!$value){
      this.onFetchAll();
    }
  }

  toggleDeleteMode() {
    this.isDeleteModeEnabled = !this.isDeleteModeEnabled;
  }
  toggleSearchMode() {
    this.isSearchModeEnabled = !this.isSearchModeEnabled;
  }
  toggleAprovedMode() {
    this.isAprovedModeEnabled = !this.isAprovedModeEnabled;
  }

  onFetchAll(){
    this.loading = true;
    this.service.fetchAll()
      .map(data => data.collection)
      .subscribe(collection => {
        this.storage.collection = collection;
        return this._collection.next(Object.assign({}, this.storage).collection);
      }, error => console.log('não foi possível carregar lista')
      , () => this.loading = false);
  }
  onSearch(collection:Simposio[]) {
    this.storage.collection = collection;
    this._collection.next(Object.assign({}, this.storage).collection);
  }

  onAproved(entity: Simposio) {
    let status = !entity.is_active;
    this.loading = true;
    this.aprovedSubscription = this.service.activate(entity.id, +status)
      .filter((params) => params.id !== undefined)
      .mergeMap((params) => this.service.fetch(params.id))
      .map(response => response.item)
      .subscribe(modal => {
        this.storage.collection = this.storage.collection.map(item => (item.id == modal.id) ? modal : item);
        this._collection.next(Object.assign({}, this.storage).collection);
      }, (error) => { }, () => this.loading = false);
  }


  /**
   * Abre confirmação de remoção
   * @param entity Simposio
   */
  onDeleteItem(entity: Simposio) {
    this.storage.selected = entity;
    this.modalDelete.open();
  }

  /**
   * Após confirmado, remove o item
   */
  onDeleteConfirmation() {
    this.loading = true;
    this.service.delete(this.storage.selected.id)
      .subscribe(data => {
        if (data.success) {
          this.onDeleteComplete();
        }
      }, (error: any) => { console.log(error); }
      , () => this.loading = false);
  }

  /**
   * Após banco de dados remove da lista
   */
  onDeleteComplete() {
    // this.collection.splice(this.collection.indexOf(this.selected), 1);
    this.storage.collection.splice(this.storage.collection.indexOf(this.storage.selected), 1);
    this._collection.next(Object.assign({}, this.storage).collection);
    this.modalDelete.close();
    this.storage.selected = null;
  }

}
