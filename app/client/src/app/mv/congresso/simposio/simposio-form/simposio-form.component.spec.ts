import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioFormComponent } from './simposio-form.component';

describe('SimposioFormComponent', () => {
  let component: SimposioFormComponent;
  let fixture: ComponentFixture<SimposioFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
