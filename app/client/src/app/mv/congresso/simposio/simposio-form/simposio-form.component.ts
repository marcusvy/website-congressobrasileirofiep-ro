import { Component, OnInit, Output, EventEmitter, HostBinding, ViewChild, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Simposio } from './../simposio';
import { SimposioService } from './../simposio.service';
import { Eixo } from './../../eixo/eixo';
import { EixoService } from './../../eixo/eixo.service';
import { Proponente } from './../../proponente/proponente';
import { ProponenteService } from './../../proponente/proponente.service';


import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'mv-simposio-form',
  templateUrl: './simposio-form.component.html',
  styleUrls: ['./simposio-form.component.scss'],
})
export class SimposioFormComponent implements OnInit, OnDestroy {

  private _paramsSubscription
  private _saveSubscription: Subscription;
  private _eixosSubscription: Subscription;
  private _eixos: Subject<Eixo[]> = new Subject<Eixo[]>();
  rxForm: FormGroup;
  eixos: Observable<Eixo[]> = this._eixos.asObservable();
  selected: Simposio;
  selectedId: number = 0;
  isSubmited = false;
  message: string;

  proponente_titulos = [];
  proponente_vinculos = [];
  hasProponente2 = false;

  constructor(
    private fb: FormBuilder,
    private service: SimposioService,
    private eixoService: EixoService,
    private proponenteService: ProponenteService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.addControls();

    this._eixosSubscription = this.eixoService.fetchAll()
      .map(result => result.collection)
      .subscribe((eixos) => {
        this._eixos.next(eixos);
      });
    this.proponente_titulos = this.proponenteService.fetchTitulos();
    this.proponente_vinculos = this.proponenteService.fetchVinculos();

    this._paramsSubscription = this.route.params.map(params => {
      this.selectedId = +params.id;
      return params;
    })
      .filter(params => (params.id !== undefined))
      .mergeMap(params => this.service.fetch(params.id))
      .map(response => response.item)
      .subscribe((entity) => this.populate(entity));
  }

  ngOnDestroy() {
    if (this._paramsSubscription) {
      this._paramsSubscription.unsubscribe();
    }
    if (this._saveSubscription) {
      this._saveSubscription.unsubscribe();
    }
    this._eixosSubscription.unsubscribe();
    this.rxForm = null;
  }

  addControls() {
    this.rxForm = this.fb.group({
      eixo: ['', Validators.required],
      titulo: ['', Validators.required],
      resumo: ['', Validators.required],
      referencia: ['', Validators.required],
      proponentes: this.fb.array([
        this.initProponenteControls()
      ])
      //proponente1

      //proponente2
      // nome2: [''],
      // email2: [''],
      // titulacao2: [''],
      // vinculo2: [''],
      // instituicao_sigla2: [''],
      // instituicao_nome2: [''],

      // hasProp2: [false]
    });

    // this.rxForm.controls.hasProp2.valueChanges.subscribe((value) => {
    //   this.hasProponente2 = value;
    // });
  }

  initProponenteControls() {
    return this.fb.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      titulacao: ['', Validators.required],
      vinculo: ['', Validators.required],
      instituicao_sigla: ['', Validators.required],
      instituicao_nome: ['', Validators.required],
    });
  }

  addProponenteControls() {
    const control = <FormArray>this.rxForm.controls['proponentes'];
    control.push(this.initProponenteControls());
  }

  removeProponenteControls(position: number) {
    const control = <FormArray>this.rxForm.controls['proponentes'];
    control.removeAt(position);
  }

  populate(entity) {
    let proponentes = [];

    proponentes.push({
      nome: entity.proponente.nome,
      email: entity.proponente.email,
      titulacao: entity.proponente.titulacao,
      vinculo: entity.proponente.vinculo,
      instituicao_sigla: entity.proponente.instituicao_sigla,
      instituicao_nome: entity.proponente.instituicao_nome
    });

    if (entity.proponente2 !== null) {
      this.addProponenteControls();
      proponentes.push({
        nome: entity.proponente2.nome,
        email: entity.proponente2.email,
        titulacao: entity.proponente2.titulacao,
        vinculo: entity.proponente2.vinculo,
        instituicao_sigla: entity.proponente2.instituicao_sigla,
        instituicao_nome: entity.proponente2.instituicao_nome
      });
    }

    this.rxForm.setValue({
      eixo: entity.eixo.id,
      titulo: entity.titulo,
      resumo: entity.resumo,
      referencia: entity.referencia,
      proponentes: proponentes
    });
  }


  prepare() {
    let data = this.rxForm.value;
    return this.fill(data);
  }


  fill(data) {

    let $proponente: Proponente = {
      nome: data['proponentes'][0]['nome'],
      email: data['proponentes'][0]['email'],
      titulacao: data['proponentes'][0]['titulacao'],
      vinculo: data['proponentes'][0]['vinculo'],
      instituicao_sigla: data['proponentes'][0]['instituicao_sigla'],
      instituicao_nome: data['proponentes'][0]['instituicao_nome'],
    };

    let $proponente2: Proponente = (data.proponentes.length > 1) ? {
      nome: data['proponentes'][1]['nome'],
      email: data['proponentes'][1]['email'],
      titulacao: data['proponentes'][1]['titulacao'],
      vinculo: data['proponentes'][1]['vinculo'],
      instituicao_sigla: data['proponentes'][1]['instituicao_sigla'],
      instituicao_nome: data['proponentes'][1]['instituicao_nome'],
    } : null;

    let $entity: Simposio = {
      titulo: data['titulo'],
      resumo: data['resumo'],
      eixo: data['eixo'],
      referencia: data['referencia'],
      proponente: $proponente,
      proponente2: $proponente2
    };
    return $entity;
  }

  onSubmit() {
    let $entity = this.prepare();
    if (this.rxForm.valid) {
      this.onSave($entity);
    }
  }

  onSave(entity) {
    if (this.selectedId) {
      entity.id = this.selectedId;
      this.service.update(entity).subscribe((data) => { this.onSaveSuccess(data) });
    } else {
      this.service.create(entity).subscribe((data) => { this.onSaveSuccess(data) });
    }
  }

  onReset() {
    this.rxForm.markAsPristine();
    this.rxForm.reset();
    this.isSubmited = false;
  }

  onSaveSuccess(data) {
    if (data.success) {
      this.message = data.message;
      this.isSubmited = true;
    };
  }
}
