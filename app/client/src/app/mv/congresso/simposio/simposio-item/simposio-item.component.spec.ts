import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioItemComponent } from './simposio-item.component';

describe('SimposioItemComponent', () => {
  let component: SimposioItemComponent;
  let fixture: ComponentFixture<SimposioItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
