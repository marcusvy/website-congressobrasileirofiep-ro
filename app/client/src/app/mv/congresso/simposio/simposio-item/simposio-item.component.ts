import { Component, OnInit, Input } from '@angular/core';

import { Simposio } from './../simposio';

@Component({
  selector: 'mv-simposio-item',
  templateUrl: './simposio-item.component.html',
  styleUrls: ['./simposio-item.component.scss']
})
export class SimposioItemComponent implements OnInit {

  @Input() entity:Simposio;
  detail = false;

  constructor() { }

  ngOnInit() {

  }

  toggleDetail(){
    this.detail = !this.detail;
  }

}
