import { Eixo } from './../eixo/eixo';
import { Proponente } from './../proponente/proponente';

export interface Simposio {
  id?: number;
  titulo?:string;
  resumo?:string;
  is_active?:boolean;
  eixo?:Eixo;
  referencia?:string;
  proponente?:Proponente;
  proponente2?:Proponente;
  hasProp2?:boolean;
}
