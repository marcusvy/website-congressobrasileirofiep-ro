import { TestBed, inject } from '@angular/core/testing';

import { SimposioService } from './simposio.service';

describe('SimposioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SimposioService]
    });
  });

  it('should be created', inject([SimposioService], (service: SimposioService) => {
    expect(service).toBeTruthy();
  }));
});
