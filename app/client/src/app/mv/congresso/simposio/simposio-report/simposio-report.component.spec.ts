import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioReportComponent } from './simposio-report.component';

describe('SimposioReportComponent', () => {
  let component: SimposioReportComponent;
  let fixture: ComponentFixture<SimposioReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
