import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs/Rx';

import { SimposioService } from './../simposio.service';

@Component({
  selector: 'mv-simposio-report',
  templateUrl: './simposio-report.component.html',
  styleUrls: ['./simposio-report.component.scss']
})
export class SimposioReportComponent implements OnInit, OnDestroy {

  private subscriptionFetch: Subscription;
  collection = [];

  constructor(
    private service: SimposioService,
  ) { }

  ngOnInit() {
    this.subscriptionFetch = this.service.fetchAll()
      .subscribe((data) => {
        this.collection = data.collection;
      });
  }

  ngOnDestroy() {
    this.subscriptionFetch.unsubscribe();
  }

}
