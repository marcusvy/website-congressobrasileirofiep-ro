import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioComponent } from './simposio.component';

describe('SimposioComponent', () => {
  let component: SimposioComponent;
  let fixture: ComponentFixture<SimposioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
