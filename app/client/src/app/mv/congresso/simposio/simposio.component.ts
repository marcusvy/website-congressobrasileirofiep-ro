import { Component, OnInit } from '@angular/core';

import { SimposioService } from './simposio.service';

@Component({
  selector: 'mv-simposio',
  templateUrl: './simposio.component.html',
  styleUrls: ['./simposio.component.scss']
})
export class SimposioComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
