import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs/Rx';

import { SimposioService } from './../simposio.service';

@Component({
  selector: 'mv-simposio-detail',
  templateUrl: './simposio-detail.component.html',
  styleUrls: ['./simposio-detail.component.scss']
})
export class SimposioDetailComponent implements OnInit, OnDestroy {

  private subscriptionFetch: Subscription;
  private subscriptionParams: Subscription;
  entity;

  constructor(
    private route: ActivatedRoute,
    private service: SimposioService
  ) { }

  ngOnInit() {
    this.subscriptionParams = this.route.params
      .subscribe((params) => {
        if (params.id) {
          this.subscriptionFetch = this.service.fetch(+params.id)
            .map(result => result.item)
            .subscribe(item => this.entity = item);
        }
      })
  }

  ngOnDestroy() {
    if(this.subscriptionFetch){
      this.subscriptionFetch.unsubscribe();
    }
    if(this.subscriptionParams){
      this.subscriptionParams.unsubscribe();
    }
  }

}
