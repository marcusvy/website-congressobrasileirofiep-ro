import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimposioDetailComponent } from './simposio-detail.component';

describe('SimposioDetailComponent', () => {
  let component: SimposioDetailComponent;
  let fixture: ComponentFixture<SimposioDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimposioDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimposioDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
