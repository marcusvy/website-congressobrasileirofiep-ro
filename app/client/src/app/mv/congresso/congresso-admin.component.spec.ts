import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongressoAdminComponent } from './congresso-admin.component';

describe('CongressoAdminComponent', () => {
  let component: CongressoAdminComponent;
  let fixture: ComponentFixture<CongressoAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongressoAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongressoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
