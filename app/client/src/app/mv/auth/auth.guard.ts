import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, CanLoad, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
    // this.authService.isAuthenticated()
    //   .subscribe((status) => {
    //   });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.checkAuthentication(next).do(data=>data);
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.canActivate(childRoute,state);
  }

  canLoad(route: Route): Observable<boolean> | boolean {
    return true;
  }

  checkAuthentication(router) {
    return this.authService.isAuthenticated()
      .map((status) => {
        if(!status) {
          this.router.navigate(['auth']);
        }
        return status;
      });
  }

}
