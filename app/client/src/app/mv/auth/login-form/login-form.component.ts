import { Component, OnInit, Output, EventEmitter, HostBinding, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'mv-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {


  public formLogin: FormGroup;
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  message;

  constructor(private fb: FormBuilder) { }

  hasMessage() {
    return (this.message!=null);
  }

  ngOnInit() {
    this.formLogin = this.fb.group({
      credential: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    if (this.formLogin.valid) {
      this.onSave.emit(this.formLogin.value);
    }
  }
}
