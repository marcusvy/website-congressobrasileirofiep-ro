import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class AuthService {

  private _authSource = new BehaviorSubject<boolean>(this.hasToken());

  constructor() {

  }

  private hasToken():boolean {
    return !!localStorage.getItem('user');
  }

  isAuthenticated(): Observable<boolean> {
    return this._authSource.asObservable();
  }

  register(model) {
    let storageData = model;
    // localStorage.setItem('user-data', JSON.stringify(storageData));
  }

  authenticate(model) {
    // localStorage.setItem('user', JSON.stringify(storageData));
    // this._authSource.next(storageData);
  }

  logout() {
    let storageData = false;
    localStorage.removeItem('user');
    this._authSource.next(storageData);
  }

}
