import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AuthService } from './auth.service';

@Component({
  selector: 'mv-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  host: {
    '[class.is-fullscreen]': 'true'
  }
})
export class AuthComponent implements OnInit {

  isAuthenticated;
  tabActive:number = 0;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {

  }
  register($event) {
    this.authService.register($event);
  }

  login($event){
    this.authService.authenticate($event);
    this.router.navigate(['congresso']);
    return true;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['auth']);
    return false;
  }
}
