import { Component, OnInit, Output, EventEmitter, HostBinding, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'mv-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {


  public formRegistro: FormGroup;
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  message;
  roles = [
    {id: 4, name: 'Ouvinte'},
    {id: 3, name: 'Proponente'}
  ];

  constructor(private fb: FormBuilder) { }

  hasMessage() {
    return (this.message!=null);
  }

  ngOnInit() {
    this.formRegistro = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      credential: ['', Validators.required],
      password: ['', Validators.required],
      role: [''],
    });
  }

  register() {
    if (this.formRegistro.valid) {
      this.onSave.emit(this.formRegistro.value);
    }
  }
}
