import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from './../core/core.module';
import { UiModule } from './../ui/ui.module';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { AuthGuard } from './auth.guard';
// import { AuthService } from './auth.service';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    AuthRoutingModule,
    UiModule
  ],
  declarations: [
    AuthComponent,
    LoginFormComponent,
    RegisterFormComponent
  ],
  providers: [
    // AuthService,
    AuthGuard,
  ]
})
export class AuthModule { }
