import { MvOrionPage } from './app.po';

describe('mv-orion App', () => {
  let page: MvOrionPage;

  beforeEach(() => {
    page = new MvOrionPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to mv!!');
  });
});
