<?php

namespace User\Entity;

use Core\Doctrine\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserPerfil
 *
 * @ORM\Table(name="mv_user_perfil", indexes={@ORM\Index(name="fk_users_perfil_avatar", columns={"avatar"})})
 * @ORM\Entity(repositoryClass="User\Repository\UserPerfilRepository")
 */
class UserPerfil
  extends AbstractEntity
{
  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="name", type="string", length=200, nullable=false)
   */
  private $name;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="birthday", type="datetime", nullable=true)
   */
  private $birthday;

  /**
   * @var string
   *
   * @ORM\Column(name="birth_place", type="string", length=50, nullable=true)
   */
  private $birthPlace;

  /**
   * @var string
   *
   * @ORM\Column(name="nationality", type="string", length=50, nullable=true)
   */
  private $nationality;

  /**
   * @var string
   *
   * @ORM\Column(name="address_number", type="string", length=20, nullable=true)
   */
  private $addressNumber;

  /**
   * @var string
   *
   * @ORM\Column(name="address_street", type="string", length=100, nullable=true)
   */
  private $addressStreet;

  /**
   * @var string
   *
   * @ORM\Column(name="address_district", type="string", length=100, nullable=true)
   */
  private $addressDistrict;

  /**
   * @var string
   *
   * @ORM\Column(name="phone_personal", type="string", length=12, nullable=true)
   */
  private $phonePersonal;

  /**
   * @var string
   *
   * @ORM\Column(name="phone_home", type="string", length=12, nullable=true)
   */
  private $phoneHome;

  /**
   * @var string
   *
   * @ORM\Column(name="phone_work", type="string", length=12, nullable=true)
   */
  private $phoneWork;

  /**
   * @var string
   *
   * @ORM\Column(name="postal_code", type="string", length=12, nullable=true)
   */
  private $postalCode;

  /**
   * @var string
   *
   * @ORM\Column(name="city", type="string", length=50, nullable=true)
   */
  private $city;

  /**
   * @var string
   *
   * @ORM\Column(name="state", type="string", length=50, nullable=true)
   */
  private $state;

  /**
   * @var string
   *
   * @ORM\Column(name="country", type="string", length=50, nullable=true)
   */
  private $country;

  /**
   * @var string
   *
   * @ORM\Column(name="sociallinks", type="text", length=65535, nullable=true)
   */
  private $sociallinks;

  /**
   * @var \Midia\Entity\Image
   *
   * @ORM\ManyToOne(targetEntity="Midia\Entity\Image")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="avatar", referencedColumnName="id")
   * })
   */
  private $avatar;

  /**
   * @return int
   */
  public function getId(): int
  {
    return $this->id;
  }

  /**
   * @param int $id
   * @return UserPerfil
   */
  public function setId(int $id): UserPerfil
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   * @return UserPerfil
   */
  public function setName(string $name): UserPerfil
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getBirthday(): \DateTime
  {
    return $this->birthday;
  }

  /**
   * @param \DateTime $birthday
   * @return UserPerfil
   */
  public function setBirthday(\DateTime $birthday): UserPerfil
  {
    $this->birthday = $birthday;
    return $this;
  }

  /**
   * @return string
   */
  public function getBirthPlace(): string
  {
    return $this->birthPlace;
  }

  /**
   * @param string $birthPlace
   * @return UserPerfil
   */
  public function setBirthPlace(string $birthPlace): UserPerfil
  {
    $this->birthPlace = $birthPlace;
    return $this;
  }

  /**
   * @return string
   */
  public function getNationality(): string
  {
    return $this->nationality;
  }

  /**
   * @param string $nationality
   * @return UserPerfil
   */
  public function setNationality(string $nationality): UserPerfil
  {
    $this->nationality = $nationality;
    return $this;
  }

  /**
   * @return string
   */
  public function getAddressNumber(): string
  {
    return $this->addressNumber;
  }

  /**
   * @param string $addressNumber
   * @return UserPerfil
   */
  public function setAddressNumber(string $addressNumber): UserPerfil
  {
    $this->addressNumber = $addressNumber;
    return $this;
  }

  /**
   * @return string
   */
  public function getAddressStreet(): string
  {
    return $this->addressStreet;
  }

  /**
   * @param string $addressStreet
   * @return UserPerfil
   */
  public function setAddressStreet(string $addressStreet): UserPerfil
  {
    $this->addressStreet = $addressStreet;
    return $this;
  }

  /**
   * @return string
   */
  public function getAddressDistrict(): string
  {
    return $this->addressDistrict;
  }

  /**
   * @param string $addressDistrict
   * @return UserPerfil
   */
  public function setAddressDistrict(string $addressDistrict): UserPerfil
  {
    $this->addressDistrict = $addressDistrict;
    return $this;
  }

  /**
   * @return string
   */
  public function getPhonePersonal(): string
  {
    return $this->phonePersonal;
  }

  /**
   * @param string $phonePersonal
   * @return UserPerfil
   */
  public function setPhonePersonal(string $phonePersonal): UserPerfil
  {
    $this->phonePersonal = $phonePersonal;
    return $this;
  }

  /**
   * @return string
   */
  public function getPhoneHome(): string
  {
    return $this->phoneHome;
  }

  /**
   * @param string $phoneHome
   * @return UserPerfil
   */
  public function setPhoneHome(string $phoneHome): UserPerfil
  {
    $this->phoneHome = $phoneHome;
    return $this;
  }

  /**
   * @return string
   */
  public function getPhoneWork(): string
  {
    return $this->phoneWork;
  }

  /**
   * @param string $phoneWork
   * @return UserPerfil
   */
  public function setPhoneWork(string $phoneWork): UserPerfil
  {
    $this->phoneWork = $phoneWork;
    return $this;
  }

  /**
   * @return string
   */
  public function getPostalCode(): string
  {
    return $this->postalCode;
  }

  /**
   * @param string $postalCode
   * @return UserPerfil
   */
  public function setPostalCode(string $postalCode): UserPerfil
  {
    $this->postalCode = $postalCode;
    return $this;
  }

  /**
   * @return string
   */
  public function getCity(): string
  {
    return $this->city;
  }

  /**
   * @param string $city
   * @return UserPerfil
   */
  public function setCity(string $city): UserPerfil
  {
    $this->city = $city;
    return $this;
  }

  /**
   * @return string
   */
  public function getState(): string
  {
    return $this->state;
  }

  /**
   * @param string $state
   * @return UserPerfil
   */
  public function setState(string $state): UserPerfil
  {
    $this->state = $state;
    return $this;
  }

  /**
   * @return string
   */
  public function getCountry(): string
  {
    return $this->country;
  }

  /**
   * @param string $country
   * @return UserPerfil
   */
  public function setCountry(string $country): UserPerfil
  {
    $this->country = $country;
    return $this;
  }

  /**
   * @return string
   */
  public function getSociallinks(): string
  {
    return $this->sociallinks;
  }

  /**
   * @param string $sociallinks
   * @return UserPerfil
   */
  public function setSociallinks(string $sociallinks): UserPerfil
  {
    $this->sociallinks = $sociallinks;
    return $this;
  }

  /**
   * @return \Midia\Entity\Image
   */
  public function getAvatar(): \Midia\Entity\Image
  {
    return $this->avatar;
  }

  /**
   * @param \Midia\Entity\Image $avatar
   * @return UserPerfil
   */
  public function setAvatar(\Midia\Entity\Image $avatar): UserPerfil
  {
    $this->avatar = $avatar;
    return $this;
  }


}

