<?php
namespace User\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Hydrator;
use User\Entity\UserPerfil;

class Address
  extends Fieldset
  implements InputFilterProviderInterface
{
  protected $serviceLocator;

  /**
   * Inicialização
   */
  public function init()
  {

    $this->setHydrator(new Hydrator\ClassMethods(false));
    $this->setObject(new UserPerfil());

    $this->add(array(
      'name' => 'addressStreet',
      'type' => 'Text',
      'options' => [
        'label' => 'Logradouro'
      ]
    ));

    $this->add(array(
      'name' => 'addressNumber',
      'type' => 'Text',
      'options' => [
        'label' => 'Número'
      ]
    ));

    $this->add(array(
      'name' => 'addressDistrict',
      'type' => 'Text',
      'options' => [
        'label' => 'Bairro/Setor'
      ]
    ));


    $this->add(array(
      'name' => 'postalCode',
      'type' => 'Text',
      'options' => [
        'label' => 'CEP'
      ]
    ));

    $this->add(array(
      'name' => 'city',
      'type' => 'Text',
      'options' => [
        'label' => 'Cidade'
      ]
    ));

    $this->add(array(
      'name' => 'state',
      'type' => 'Text',
      'options' => [
        'label' => 'Estado'
      ]
    ));

    $this->add(array(
      'name' => 'country',
      'type' => 'Text',
      'options' => [
        'label' => 'País'
      ]
    ));
  }

  public function getInputFilterSpecification()
  {
    return array(

      'addressStreet' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 100
          ]],
        ],
      ),

      'addressNumber' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 20
          ]],
        ],
      ),

      'addressDistrict' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 100
          ]],
        ],
      ),

      'postalCode' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 8,
            'max' => 8
          ]],
        ],
      ),

      'city' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 100
          ]],
        ],
      ),

      'state' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 50
          ]],
        ],
      ),

      'country' => array(
        'required' => false,
        'filters' => [
          ['name' => 'Zend\Filter\StringTrim'],
          ['name' => 'Zend\Filter\StripTags'],
        ],
        'validators' => [
          ['name' => 'StringLength', 'options' => [
            'min' => 1,
            'max' => 50
          ]],
        ],
      ),

    );
  }

  /**
   * @param ServiceLocatorInterface $serviceLocator
   */
  public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
  {
    $this->serviceLocator = $serviceLocator;
  }

  public function getServiceLocator()
  {
    return $this->serviceLocator;
  }
}
