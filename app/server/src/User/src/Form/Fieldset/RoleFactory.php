<?php
namespace User\Form\Fieldset;

use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use User\Service\UserRoleService;

class RoleFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $service = $container->get(UserRoleService::class);
    return new Role($service);
  }
}
