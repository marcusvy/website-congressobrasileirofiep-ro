<?php
namespace User\Form;

use Zend\Form\Form;

class Login
  extends Form
{
  public function init()
  {
    $this->add(array(
      'name' => 'login',
      'type' => 'User\Form\Fieldset\Login',
    ));

    $this->add([
      'name' => 'remember',
      'type' => 'Checkbox',
      'options'=>[
        'label'=>'Lembrar meu registro'
      ]
    ]);
  }

  public function getInputFilterSpecification()
  {
    return array(
      'remember' => array(
        'filters' => [
          ['name' => 'Zend\Filter\Boolean']
        ]
      )
    );
  }
}
