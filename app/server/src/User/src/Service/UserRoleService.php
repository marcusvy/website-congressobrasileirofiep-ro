<?php
namespace User\Service;

use Core\Service\AbstractService;
use User\Entity\UserRole;
use User\Repository\UserRoleRepository;

class UserRoleService
  extends AbstractService
  implements UserRoleServiceInterface
{
  protected $entity = UserRole::class;

  public function getNamesForSelect(){
    /** @var UserRoleRepository $repository */
    $repository = $this->entityManager->getRepository(UserRole::class);
    $collection = $repository->getNamesForSelect();
    $result = [];
    foreach ($collection as $object) {
      $result[($object->getId())] = $object->getName();
    }
    return $result;
  }
}
