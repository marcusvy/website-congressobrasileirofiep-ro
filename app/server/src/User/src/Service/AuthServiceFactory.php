<?php

namespace User\Service;

use Interop\Container\ContainerInterface;
use User\Adapter\AuthAdapter;
use User\Storage\AuthSession;

class AuthServiceFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $storage = new AuthSession();
    $adapter = $container->get(AuthAdapter::class);
    return new AuthService($storage, $adapter);
  }
}
