<?php
namespace User\Service;

use Core\Service\ServiceInterface;

interface UserPerfilServiceInterface
  extends ServiceInterface
{
  /**
   * Get the data from Resquest and tranform for service usage
   * @param array $data
   * @return mixed
   */
  public function dataNormalization(array $data);
}
