<?php
namespace User\Service;

use Doctrine\ORM\EntityManager;
use Core\Service\AbstractService;
use User\Entity\User;
use User\Entity\UserPerfil;
use User\Entity\UserRole;
use Zend\Hydrator\ClassMethods;


/**
 * Class UserService
 * @package User\Service
 */
class UserService
  extends AbstractService
  implements UserServiceInterface
{
  /** @var \User\Entity\User */
  protected $entity = User::class;

  /** @var  \User\Service\UserPerfilService */
  protected $perfilService;

  public function __construct(
    EntityManager $entityManager,
    UserPerfilServiceInterface $perfilService
  ){
    $this->entityManager = $entityManager;
    $this->perfilService = $perfilService;
  }

  public function findAll()
  {
    $result = [];
    $list = $this->getEntityManger()->getRepository($this->entity)->findAll();
    /** @var \User\Entity\User $entity */
    foreach ($list as $entity) {
      $result[$entity->getId()] = $entity->toArray();
    }
    return $result;
  }

  public function insert(array $data)
  {
    // Perfil
    $perfil = $this->perfilService->insert($data);

    //Usuário
    $user = new User($this->dataNormalization($data));

    $role = isset($data['role']) ? $data['role'] : null;
    $role = $this->getEntityManger()
      ->getRepository(UserRole::class)
      ->find($role['id']);

    $user->setRole($role)
      ->setPerfil($perfil)
      ->encriptPassword();

    $this->getEntityManger()->persist($user);
    $this->getEntityManger()->flush();
    return $user;
  }

  public function update($id, $data)
  {
    $dataNormalized = $this->dataNormalization($data);

    /** @var \User\Entity\User $user */
    $user = $this->getEntityManger()->getReference(User::class, $id);
    (new ClassMethods(false))->hydrate($this->dataNormalization($data), $user);
    $this->getEntityManger()->persist($user);

    $perfil = $this->getEntityManger()
      ->getReference(UserPerfil::class, $user->getPerfil()->getId());
    (new ClassMethods(false))->hydrate($this->perfilService->dataNormalization($data), $perfil);
    $this->getEntityManger()->persist($perfil);

    $this->getEntityManger()->flush();
    return $user;
  }

  public function delete($id)
  {
    $user = $this->getEntityManger()
      ->getReference($this->entity, $id);
    if ($user) {
      $perfil = $this->getEntityManger()
        ->getReference(UserPerfil::class, $user->getPerfil()->getId());
      if ($perfil) {
        $this->getEntityManger()->remove($perfil);
      }
      $this->getEntityManger()->remove($user);
      $this->getEntityManger()->flush();
      return $id;
    }
    return 0;
  }

  public function status($id, $status)
  {
    $user = $this->getEntityManger()
      ->getReference($this->entity, $id);
    if ($user) {
      $data = ['status' => $status];
      (new ClassMethods(false))->hydrate($data, $user);
      $this->getEntityManger()->persist($user);
      $this->getEntityManger()->flush();
      return $user;
    }
    return false;
  }

  public function activate($id, $key)
  {
    /** @var \User\Entity\user $user */
    $user = $this->getEntityManger()
      ->getReference($this->entity, $id);
    if ($user && $key==$user->getActivationKey()) {
      $user->setActive(1);
      $user->setActivationKey(null);
      $this->getEntityManger()->persist($user);
      $this->getEntityManger()->flush();
      return $user;
    }
    return false;
  }

  /**
   * Normaliza os dados injetados pelos Fieldsets
   * @param array $data
   * @return array
   */
  public function dataNormalization(array $data)
  {
    return array_merge(
      isset($data['login']) ? $data['login'] : [],
      isset($data['activation']) ? $data['activation'] : []
    );
  }


}
