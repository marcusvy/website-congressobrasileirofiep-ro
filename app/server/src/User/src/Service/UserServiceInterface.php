<?php
namespace User\Service;

use Core\Service\ServiceInterface;

interface UserServiceInterface
  extends ServiceInterface
{
  /**
   * Atualiza status de ativação do usuário
   * @param int $id
   * @param int|string $status Actual status
   * @return \User\Entity\User
   */
  public function status($id, $status);

  /**
   * Ativa através de um código de ativação
   * @param int|string $id
   * @param string $key Key of activation
   * @return \User\Entity\User
   */
  public function activate($id, $key);

  /**
   * Normaliza os dados injetados pelos Fieldsets
   * @param array $data
   * @return array
   */
  public function dataNormalization(array $data);
}
