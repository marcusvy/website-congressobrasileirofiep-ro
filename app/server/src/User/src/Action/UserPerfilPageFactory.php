<?php

namespace User\Action;

use Interop\Container\ContainerInterface;
use User\Service\UserPerfilService;
use Zend\Expressive\Router\RouterInterface;
use Doctrine\ORM\EntityManager;

class UserPerfilPageFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $router   = $container->get(RouterInterface::class);
    $entityManager = $container->get(EntityManager::class);
    $service = $container->get(UserPerfilService::class);

    return new UserPerfilPageAction($router, $entityManager, $service);
  }
}
