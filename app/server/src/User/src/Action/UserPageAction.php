<?php

namespace User\Action;

use Core\Action\AbstractRestPageAction;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use User\Entity\User;

class UserPageAction
  extends AbstractRestPageAction
  implements ServerMiddlewareInterface
{
  protected $entity = User::class;
}
