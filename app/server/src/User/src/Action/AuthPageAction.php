<?php

namespace User\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Form\Form;
use Zend\Expressive\ZendView\ZendViewRenderer;
use Zend\View\Model\ViewModel;

class AuthPageAction implements MiddlewareInterface
{
    /**
     * @var ZendViewRenderer
     */
    private $template;
    private $form;
    private $router;

    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template
        // ,
        // Form $form

    ){
      $this->router = $router;
      $this->template = $template;
    //   $this->form = $form;
    }
    
    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        // $response = $delegate->process($request);
        // return $response;
        $layout = new ViewModel([]);
        $layout->setTemplate('layout::default');

        $menu = new ViewModel();
        $menu->setTemplate('layout::menu');

        $layout->addChild($menu);

        $data = [
            'layout' => $layout
        ];
        return new HtmlResponse($this->template->render('user::auth-page', $data));
    }
}