<?php

namespace User\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Doctrine\ORM\EntityManager;
use User\Service\UserService;
use User\Form\Login;

class AuthPageFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $router = $container->get(RouterInterface::class);
    $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

    // $service = $container->get(UserService::class);
    // $formElementManager = $container->get('FormElementManager');
    // $form = $formElementManager->get(Login::class);

    return new AuthPageAction($router, $template);
  }
}
