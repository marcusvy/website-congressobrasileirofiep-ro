<?php

namespace User\Action;

use Interop\Container\ContainerInterface;
use User\Service\UserRoleService;
use Zend\Expressive\Router\RouterInterface;
use Doctrine\ORM\EntityManager;

class UserRolePageFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $router   = $container->get(RouterInterface::class);
    $entityManager = $container->get(EntityManager::class);
    $service = $container->get(UserRoleService::class);

    return new UserRolePageAction($router, $entityManager, $service);
  }
}
