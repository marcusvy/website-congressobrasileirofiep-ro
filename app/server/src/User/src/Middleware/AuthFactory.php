<?php
namespace User\Middleware;

use Psr\Container\ContainerInterface;
use User\Service\AuthService;

class AuthFactory {

    public function __invoke(ContainerInterface $container)
    {
        // $em = $container->get(EntityManager::class);
        $service = $container->get(AuthService::class);
        return new Auth($service);
    }
}