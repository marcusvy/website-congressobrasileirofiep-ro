<?php

namespace User\Middleware;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Diactoros\Response\RedirectResponse;

class Auth implements MiddlewareInterface
{
    /**
     * @var AuthenticationServiceInterface
     */
    private $service;

    public function __construct(AuthenticationServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        
        
        // if(!$this->service->hasIdentity()){
        //     return new RedirectResponse('/login');
        // }
        
        return $delegate->process($request);
    }
}