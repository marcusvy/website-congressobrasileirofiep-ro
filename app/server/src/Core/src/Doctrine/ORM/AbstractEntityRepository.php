<?php
namespace Core\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;

abstract class AbstractEntityRepository
  extends EntityRepository
{
  public function getAll()
  {
    $list = $this->generatorFindAll($this->findAll());
    $data = array();

    if (version_compare(phpversion(), '5.5', '<')) {
      foreach ($list as $object) {
        $data[] = $object->toArray();
      }
    } else {
      foreach ($list as $object) {
        $data[] = $object;
      }
    }
    return $data;
  }

  protected function generatorFindAll($list)
  {
    foreach ($list as $object) {
      yield $object->toArray();
    }
  }
}
