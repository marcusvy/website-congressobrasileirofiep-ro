<?php
namespace Core\Doctrine;

use Zend\Hydrator\ClassMethods;

abstract class AbstractEntity
  implements EntityInterface
{

  /**
   * @param array $options
   */
  public function __construct($options=array()) {
    $hydrator = new ClassMethods();
    $hydrator->hydrate($options, $this);
  }

  /**
   * Transforma a entidade em um array
   * @return array
   */
  public function toArray()
  {
    $hydrator = new ClassMethods();
    $result = $hydrator->extract($this);
    return $result;
  }

  /**
   * Populate on form validation
   * @param $data
   * @return EntityInterface
   */
  public function exchangeArray($data)
  {
    $hydrator = new ClassMethods();
    return $hydrator->hydrate($data, $this);
  }

}
