<?php

namespace Congresso;

use Core\Doctrine\Helper\ConfigProviderHelper;

/**
 * The configuration provider for the Congresso module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'doctrine' => $this->getDoctrine(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
                Action\EixoAction::class => Action\EixoFactory::class,
                Action\SimposioAction::class => Action\SimposioFactory::class,
                Action\SimposioActivationAction::class => Action\SimposioActivationFactory::class,
                Action\ProponenteAction::class => Action\ProponenteFactory::class,
                Action\OuvinteAction::class => Action\OuvinteFactory::class,
                Service\EixoService::class => Service\EixoServiceFactory::class,
                Service\ProponenteService::class => Service\ProponenteServiceFactory::class,
                Service\SimposioService::class => Service\SimposioServiceFactory::class,
                Service\OuvinteService::class => Service\OuvinteServiceFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
            ],
        ];
    }


    /**
     * Returns the doctrine configuration
     *
     * @return array
     */
    public function getDoctrine()
    {
        $helper = new ConfigProviderHelper();
        return $helper->generate(
            __NAMESPACE__,
            __DIR__ . '/Entity'
        );
    }
}
