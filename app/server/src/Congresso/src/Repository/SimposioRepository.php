<?php
namespace Congresso\Repository;

use Core\Doctrine\ORM\AbstractEntityRepository;

/**
 * SimposioRepository
 */
class SimposioRepository
    extends AbstractEntityRepository
{
    public function findByTitulo($search)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from($this->getEntityName(), 's')
            ->where($qb->expr()->like('s.titulo', $qb->expr()->literal('%' . $search . '%')));
        return $qb->getQuery()->getResult();
    }
}
