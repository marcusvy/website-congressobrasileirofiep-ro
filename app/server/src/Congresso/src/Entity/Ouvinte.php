<?php

namespace Congresso\Entity;

use Core\Doctrine\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;

/**
 * Ouvinte
 *
 * @ORM\Table(
 *  name="mv_congresso_ouvinte"
 * )
 * @ORM\Entity(repositoryClass="Congresso\Repository\OuvinteRepository")
 */
class Ouvinte
    extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

      /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="situacao", type="string", length=255, nullable=false)
     */
    private $situacao;

    /**
     * @var string
     *
     * @ORM\Column(name="instituicao_sigla", type="string", length=40, nullable=false)
     */
    private $instituicaoSigla; 

    /**
     * @var string
     *
     * @ORM\Column(name="instituicao_nome", type="string", length=255, nullable=false)
     */
    private $instituicaoNome;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_fixo", type="string", length=20, nullable=true)
     */
    private $telefoneFixo;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone_celular", type="string", length=20, nullable=false)
     */
    private $telefoneCelular;


    public function __construct($options = array())
    {
        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return Ouvinte
     */
    public function setId(int $id): Ouvinte
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }
    /**
     * @param int $nome
     * @return Ouvinte
     */
    public function setNome(string $nome): Ouvinte
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }    

    /**
     * @param string $email
     * @return Ouvinte
     */
    public function setEmail($email): Ouvinte
    {   
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param string $situacao
     * @return Ouvinte
     */
    public function setSituacao($situacao): Ouvinte
    {
        $this->situacao = $situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstituicaoSigla(): string
    {
        return $this->instituicaoSigla;
    }
    /**
     * @param string $instituicaoSigla
     * @return Ouvinte
     */
    public function setInstituicaoSigla(string $instituicaoSigla): Ouvinte
    {
        $this->instituicaoSigla = $instituicaoSigla;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstituicaoNome(): string
    {
        return $this->instituicaoNome;
    }
    /**
     * @param string $instituicaoNome
     * @return Ouvinte
     */
    public function setInstituicaoNome(string $instituicaoNome): Ouvinte
    {
        $this->instituicaoNome = $instituicaoNome;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneFixo(): string
    {
        return $this->telefoneFixo;
    }

    /**
     * @param string $telefoneFixo
     * @return Ouvinte
     */
    public function setTelefoneFixo(string $telefoneFixo): Ouvinte
    {
        $this->telefoneFixo = $telefoneFixo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefoneCelular(): string
    {
        return $this->telefoneCelular;
    }

    /**
     * @param string $telefoneCelular
     * @return Ouvinte
     */
    public function setTelefoneCelular(string $telefoneCelular): Ouvinte
    {
        $this->telefoneCelular = $telefoneCelular;
        return $this;
    }



    /**
     * Transforma a entidade em um array
     * @return array
     */
    public function toArray()
    {
        $hydrator = new ClassMethods();
        $result = $hydrator->extract($this);
        return $result;
    }

}
