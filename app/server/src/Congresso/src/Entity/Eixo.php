<?php

namespace Congresso\Entity;

use Core\Doctrine\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;
 
/**
 * Eixo
 *
 * @ORM\Table(
 *  name="mv_congresso_eixo"
 * )
 * @ORM\Entity(repositoryClass="Congresso\Repository\EixoRepository")
 */
class Eixo
    extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;


    public function __construct($options = array())
    {
        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return Eixo
     */
    public function setId($id): Eixo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }
    /**
     * @param int $id
     * @return Eixo
     */
    public function setNome(string $nome): Eixo
    {
        $this->nome = $nome;
        return $this;
    }  

}