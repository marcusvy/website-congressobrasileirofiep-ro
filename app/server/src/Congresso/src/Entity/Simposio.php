<?php

namespace Congresso\Entity;

use Core\Doctrine\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;
use Congresso\Entity\Eixo;
use Congresso\Entity\Proponente;

/**
 * Simposio
 *
 * @ORM\Table(
 *  name="mv_congresso_simposio"
 * )
 * @ORM\Entity(repositoryClass="Congresso\Repository\SimposioRepository")
 */
class Simposio
    extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="resumo", type="text", length=500, nullable=false)
     */
    private $resumo;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="text", length=1500, nullable=false)
     */
    private $referencia;
    

    /**
     * @var Eixo
     *
     * @ORM\ManyToOne(targetEntity="Congresso\Entity\Eixo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_eixo", referencedColumnName="id")
     * })
     */
    private $eixo;

    /**
     * @var Proponente
     *
     * @ORM\ManyToOne(targetEntity="Congresso\Entity\Proponente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_proponente", referencedColumnName="id")
     * })
     */
    private $proponente;

    /**
     * @var Proponente
     *
     * @ORM\ManyToOne(targetEntity="Congresso\Entity\Proponente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_proponente2", referencedColumnName="id")
     * })
     */
    private $proponente2;



    public function __construct($options = array())
    {
        $this->setCreatedat(new \DateTime('now'))
            ->setUpdatedat(new \DateTime('now'));

        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Simposio
     */
    public function setId(int $id): Simposio
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedat(): \DateTime
    {
        return $this->createdat;
    }

    /**
     * @param \DateTime $createdat
     * @return Simposio
     */
    public function setCreatedat(\DateTime $createdat): Simposio
    {
        $this->createdat = $createdat;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedat(): \DateTime
    {
        return $this->updatedat;
    }

    /**
     * @param \DateTime $updatedat
     * @return Simposio
     */
    public function setUpdatedat(\DateTime $updatedat): Simposio
    {
        $this->updatedat = $updatedat;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     * @return Simposio
     */
    public function setActive(bool $active): Simposio
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return Simposio
     */
    public function setTitulo(string $titulo): Simposio
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getResumo(): string
    {
        return $this->resumo;
    }

    /**
     * @param string $resumo
     * @return Simposio
     */
    public function setResumo(string $resumo): Simposio
    {
        $this->resumo = $resumo;
        return $this;
    }

    /**
     * @return string
     */
    public function getReferencia(): string
    {
        return $this->referencia;
    }

    /**
     * @param string $referencia
     * @return Simposio
     */
    public function setReferencia(string $referencia): Simposio
    {
        $this->referencia = $referencia;
        return $this;
    }

    /**
     * @return Eixo
     */
    public function getEixo(): Eixo
    {
        return $this->eixo;
    }

    /**
     * @param Eixo $eixo
     * @return Simposio
     */
    public function setEixo($eixo): Simposio
    {
        $this->eixo = $eixo;
        return $this;
    }

    /**
     * @return Proponente
     */
    public function getProponente(): Proponente
    {
        return $this->proponente;
    }

    /**
     * @param Proponente $proponente
     * @return Simposio
     */
    public function setProponente($proponente): Simposio
    {
        $this->proponente = $proponente;
        return $this;
    }

    /**
     * @return Proponente
     */
    public function getProponente2() {
        return $this->proponente2;
    }

    /**
     * @param Proponente $proponente2
     * @return Simposio
     */
    public function setProponente2($proponente2): Simposio
    {
        $this->proponente2 = $proponente2;
        return $this;
    }

    /**
     * Transforma a entidade em um array
     * @return array
     */
    public function toArray()
    {
        $hydrator = new ClassMethods();
        $result = $hydrator->extract($this);
        $result['createdat'] = $this->getCreatedat()->format('Y-m-d H:m:s');
        $result['updatedat'] = $this->getUpdatedat()->format('Y-m-d H:m:s');
        $result['eixo'] = $this->getEixo()->toArray();
        $result['proponente'] = $this->getProponente()->toArray();
        $result['proponente2'] = (!is_null($this->getProponente2())) ? $this->getProponente2()->toArray() : null;
        return $result;
    }

}
