<?php

namespace Congresso\Entity;

use Core\Doctrine\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;
use Zend\Form\Form;

/**
 * Proponente
 *
 * @ORM\Table(
 *  name="mv_congresso_proponente"
 * )
 * @ORM\Entity(repositoryClass="Congresso\Repository\ProponenteRepository")
 */
class Proponente
    extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="titulacao", type="integer", nullable=false)
     *
     * 1. Graduado; 
     * 2. Especialista; 
     * 3. Mestre; 
     * 4. Doutor; 
     * 5. Pós-Doutor;
     * 6. Livre Docente
     */
    private $titulacao;

    /**
     * @var integer
     *
     * @ORM\Column(name="vinculo", type="integer", nullable=false)
     *
     * 1. Pesquisador; 
     * 2. Professor; 
     * 3. Pós-Doutorando; 
     * 4. Doutorando; 
     * 5. Mestrando;
     * 6. Especializando
     * 7. Graduando
     */
    private $vinculo;

    /**
     * @var string
     *
     * @ORM\Column(name="instituicao_sigla", type="string", length=40, nullable=false)
     */
    private $instituicaoSigla;

    /**
     * @var string
     *
     * @ORM\Column(name="instituicao_nome", type="string", length=255, nullable=false)
     */
    private $instituicaoNome;


    public function __construct($options = array())
    {
        parent::__construct($options);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return Proponente
     */
    public function setId(int $id): Proponente
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }
    /**
     * @param int $nome
     * @return Proponente
     */
    public function setNome(string $nome): Proponente
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param int $email
     * @return Proponente
     */
    public function setEmail(string $email): Proponente
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulacao(): string
    {
        return $this->titulacao;
    }
    /**
     * @param int $titulacao
     * @return Proponente
     */
    public function setTitulacao(string $titulacao): Proponente
    {
        $this->titulacao = $titulacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getVinculo(): string
    {
        return $this->vinculo;
    }
    /**
     * @param int $vinculo
     * @return Proponente
     */
    public function setVinculo(int $vinculo): Proponente
    {
        $this->vinculo = $vinculo;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstituicaoSigla(): string
    {
        return $this->instituicaoSigla;
    }
    /**
     * @param string $instituicaoSigla
     * @return Proponente
     */
    public function setInstituicaoSigla(string $instituicaoSigla): Proponente
    {
        $this->instituicaoSigla = $instituicaoSigla;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstituicaoNome(): string
    {
        return $this->instituicaoNome;
    }
    /**
     * @param string $instituicaoNome
     * @return Proponente
     */
    public function setInstituicaoNome(string $instituicaoNome): Proponente
    {
        $this->instituicaoNome = $instituicaoNome;
        return $this;
    }
    
    /**
     * Transforma a entidade em um array
     * @return array
     */
    public function toArray()
    {
        $hydrator = new ClassMethods();
        $result = $hydrator->extract($this);
        return $result;
    }

}
