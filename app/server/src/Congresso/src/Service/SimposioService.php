<?php
namespace Congresso\Service;

use Core\Service\AbstractService;
use Congresso\Entity\Eixo;
use Congresso\Entity\Simposio;
use Congresso\Entity\Proponente;
use Zend\Hydrator;

class SimposioService
    extends AbstractService
{
    protected $entity = Simposio::class;

    public function create(array $data)
    {
        /** @var Simposio $entity */
        $entity = new Simposio($data);

        $dataEixo = isset($data['eixo']) ? $data['eixo'] : null;
        /** @var Eixo $eixo */
        $eixo = $this->getEntityManger()
            ->getRepository(Eixo::class)
            ->findOneBy(['id' => $dataEixo]);

        /** @var Proponente $proponente */
        $dataProponente = isset($data['proponente']) ? $data['proponente'] : null;
        $proponente = $this->getEntityManger()
            ->getRepository(Proponente::class)
            ->findOneBy(['id' => $dataProponente]);

        $dataProponente2 = isset($data['proponente2']) ? $data['proponente2'] : null;
        $proponente2 = $this->getEntityManger()
            ->getRepository(Proponente::class)
            ->findOneBy(['id' => $dataProponente2]);

        $entity->setEixo($eixo);
        $entity->setProponente($proponente);

        $entity->setProponente2($proponente2);

        $this->getEntityManger()->persist($entity);
        $this->getEntityManger()->flush();
        return $entity;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Simposio
     */
    public function update($id, $data)
    {
        /** @var Simposio $entity */
        $entity = $this->getEntityManger()->getReference($this->entity, $id); (new Hydrator\ClassMethods(false))->hydrate($data, $entity);

        /** @var Eixo $eixo */
        $dataEixo = isset($data['eixo']) ? $data['eixo'] : null;
        $eixo = $this->getEntityManger()->getReference(Eixo::class, $data['eixo']);
        $entity->setEixo($eixo);

        /** @var Proponente $proponente */
        $dataProponente = isset($data['proponente']) ? $data['proponente'] : null;
        $proponente = $this->getEntityManger()->getReference(Proponente::class, $data['proponente']);
        $entity->setProponente($proponente);

        /** @var Proponente $proponente2 */
        $dataProponente2 = isset($data['proponente2']) ? $data['proponente2'] : null;
        if (!is_null($dataProponente2)) {
            $proponente2 = $this->getEntityManger()->getReference(Proponente::class, $data['proponente2']);
            $entity->setProponente2($proponente2);
        }

        $this->getEntityManger()->persist($entity);
        $this->getEntityManger()->flush();
        return $entity;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Simposio
     */
    public function activation($id, $status)
    {
        /** @var Simposio $entity */
        $entity = $this->getEntityManger()->getReference($this->entity, $id);
        $entity->setActive($status);
        $this->getEntityManger()->persist($entity);
        $this->getEntityManger()->flush();
        return $entity;
    }

    public function searchBy($column, $search)
    {   
        /** @var $repo \Congresso\Repository\SimposioRepository */
        $repo = $this->getEntityManger()->getRepository($this->entity);
        $result = [];
        $prepare = function($entity){
            return $entity->toArray();
        };
        switch($column){
            case 'titulo': $result = $repo->findByTitulo($search); break;
        }
        if(count($result)>0){
            $result = array_map($prepare,$result);
        }
        return $result;
    }

}
