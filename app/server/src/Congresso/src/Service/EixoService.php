<?php

namespace Congresso\Service;

use Core\Service\AbstractService;
use Congresso\Entity\Eixo;

class EixoService
    extends AbstractService
{
    protected $entity = Eixo::class;
}
