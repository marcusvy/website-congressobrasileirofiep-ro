<?php

namespace Congresso\Service;

use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;

class OuvinteServiceFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $entityManager = $container->get(EntityManager::class);
    return new OuvinteService($entityManager);
  }
}
