<?php

namespace Congresso\Service;

use Core\Service\AbstractService;
use Congresso\Entity\Proponente;

class ProponenteService
    extends AbstractService
{
    protected $entity = Proponente::class;

    public function create(array $data)
    {
        $entity = new $this->entity($data);
        $this->getEntityManger()->persist($entity);
        $this->getEntityManger()->flush();
        return $entity;
    }
}
