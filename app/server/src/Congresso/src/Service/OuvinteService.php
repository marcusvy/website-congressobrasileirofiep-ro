<?php

namespace Congresso\Service;

use Core\Service\AbstractService;
use Congresso\Entity\Ouvinte;

class OuvinteService
    extends AbstractService
{
    protected $entity = Ouvinte::class;

}
