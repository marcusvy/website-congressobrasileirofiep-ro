<?php

namespace Congresso\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use Core\Action\AbstractRestPageAction;
use Core\Service\ServiceInterface;
use Zend\Json\Json;
use Congresso\Entity\Simposio;

class SimposioActivationAction implements MiddlewareInterface
{
    protected $entity = Simposio::class;
    private $entityManager;
    private $service;

    public function __construct(
        EntityManager $entityManager,
        ServiceInterface $service
    ){
        $this->entityManager = $entityManager;
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        switch ($request->getMethod()) {
            case 'OPTIONS':
            case 'GET':
                return $this->activateAction($request, $delegate);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Not found!',
        ], 404);
    }

     public function activateAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = $request->getAttribute('id') ? (int)$request->getAttribute('id') : 0;
        $status = $request->getAttribute('status') ? (int)$request->getAttribute('status') : 0;


        if ($id) {

            /** @var \Congresso\Entity\Simposio */
            $entity = $this->entityManager->getReference(Simposio::class, $id);

            try {
                $entity = $this->service->activation($id, $status);
                if ($entity) {
                    return new JsonResponse([
                        'success' => true,
                        'id' => $id,
                        'message' => 'Simpósio atualizado',
                    ]);
                }
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->errorHandler($e);
            }
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Proponentes cadastrados mas o simpósio não. Verifique novamente',
        ], 200);
    }
    protected function errorHandler(\Doctrine\DBAL\DBALException $e)
    {
        if ($e->getPrevious() && 0 === strpos($e->getPrevious()->getCode(), '23')) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Registro já existe',
            ], 200);
        } 
        else {
            throw new \Exception($e->getMessage());
        }
    }
}