<?php

namespace Congresso\Action;

use Interop\Container\ContainerInterface;
use Congresso\Service\SimposioService;
use Congresso\Service\ProponenteService;
use Doctrine\ORM\EntityManager;

class SimposioFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);
        $service = $container->get(SimposioService::class);
        $proponenteService = $container->get(ProponenteService::class);

        return new SimposioAction($em, $service, $proponenteService);
    }
}
