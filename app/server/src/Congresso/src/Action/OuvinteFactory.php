<?php

namespace Congresso\Action;

use Interop\Container\ContainerInterface;
use Congresso\Service\OuvinteService;
use Congresso\Service\ProponenteService;
use Doctrine\ORM\EntityManager;

class OuvinteFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);
        $service = $container->get(OuvinteService::class);

        return new OuvinteAction($em, $service);
    }
}
