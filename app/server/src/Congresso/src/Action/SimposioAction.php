<?php
namespace Congresso\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use Core\Action\AbstractRestPageAction;
use Core\Service\ServiceInterface;
use Zend\Json\Json;
use Congresso\Entity\Simposio;


class SimposioAction
    extends AbstractRestPageAction
    implements MiddlewareInterface
{
    protected $entity = Simposio::class;

    // protected $router;
    // protected $entityManager;
    // protected $service;
    // private $proponenteService;

    public function __construct(
        EntityManager $entityManager,
        ServiceInterface $service,
        ServiceInterface $proponenteService
    )
    {
        $this->entityManager = $entityManager;
        $this->service = $service;
        $this->proponenteService = $proponenteService;
    }

    /**
     * {@inheritDoc}
     */
    // public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    // {
    //     switch ($request->getMethod()) {
    //         case 'OPTIONS':
    //         case 'GET':
    //             $id = $request->getAttribute('id') ? (int)$request->getAttribute('id') : 0;
    //             if ($id) {
    //                 return $this->getAction($request, $delegate);
    //             } else {
    //                 return $this->listAction($request, $delegate);
    //             }
    //         case 'POST':
    //             return $this->createAction($request, $delegate);
    //         case 'DELETE':
    //             return $this->deleteAction($request, $delegate);
    //     }
    //     return new JsonResponse([
    //         'success' => false,
    //         'message' => 'Not found!',
    //     ], 404);
    // }

    public function getAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = ($request->getAttribute('id')) ? (int)$request->getAttribute('id') : 0;
        $entity = $this->service->get($id);
        if ($entity) {
            $result = $entity->toArray();
            return new JsonResponse([
                'success' => true,
                'item' => $result,
            ]);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Solicitação não encontrada!'
        ], 200);
    }

    public function listAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $collection = $this->service->list();
        return new JsonResponse([
            'success' => true,
            'collection' => $collection
        ]);
    }

    public function createAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contentType = $request->getHeader('Content-Type');
        $data = ['success' => 'false'];
        /** @var \Congresso\Entity\Proponente $entityProponente */
        $entityProponente = null;
        /** @var \Congresso\Entity\Proponente $entityProponente2 */
        $entityProponente2 = null;

        if ($contentType[0] == 'application/json') {
            $data = Json::decode($request->getBody(), Json::TYPE_ARRAY);
        }

        try {
            $entityProponente = $this->proponenteService->create($data['proponente']);
            $data['proponente'] = $entityProponente->getId();
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->errorHandler($e);
        }


        try {
            if (!is_null($data['proponente2'])) {
                $entityProponente2 = $this->proponenteService->create($data['proponente2']);
                $data['proponente2'] = $entityProponente2->getId();
            }
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->errorHandler($e);
        }

        try {
            $dataMap = [
                'eixo' => $data['eixo'],
                'titulo' => $data['titulo'],
                'resumo' => $data['resumo'],
                'referencia' => $data['referencia'],
                'proponente' => $data['proponente'],
                'proponente2' => (!is_null($entityProponente2)) ? $data['proponente2'] : null,
            ];
            $entity = $this->service->create($dataMap);
            if ($entity) {
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Simpósio submetido',
                ]);
            }
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->errorHandler($e);
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Proponentes cadastrados mas o simpósio não. Verifique novamente',
        ], 200);
    }

    public function updateAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = ($request->getAttribute($this->primaryColumn)) ? (int)$request->getAttribute($this->primaryColumn) : 0;
        $contentType = $request->getHeader('Content-Type');
        $data = ['success' => 'false'];
        /** @var \Congresso\Entity\Proponente $entityProponente */
        $entityProponente = null;
        /** @var \Congresso\Entity\Proponente $entityProponente2 */
        $entityProponente2 = null;

        if ($contentType[0] == 'application/json') {
            $data = Json::decode($request->getBody(), Json::TYPE_ARRAY);
        }


        if ($id) {

            /** @var \Congresso\Entity\Simposio */
            $entity = $this->entityManager->getReference(Simposio::class, $id);


            try {
                $entityProponente = $this->proponenteService->update(
                    $entity->getProponente()->getId(),
                    $data['proponente']
                );
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->errorHandler($e);
            }

            if (!is_null($data['proponente2'])) {
                try {
                    $entityProponente2 = $this->proponenteService->update(
                        $entity->getProponente2()->getId(),
                        $data['proponente2']
                    );

                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->errorHandler($e);
                }
            }

            try {
                $dataMap = [
                    'eixo' => $data['eixo'],
                    'titulo' => $data['titulo'],
                    'resumo' => $data['resumo'],
                    'referencia' => $data['referencia'],
                    'proponente' => $entityProponente->getId(),
                    'proponente2' => (!is_null($entityProponente2)) ? $entityProponente2->getId() : null
                ];
                $entity = $this->service->update($id, $dataMap);
                if ($entity) {
                    return new JsonResponse([
                        'success' => true,
                        'message' => 'Simpósio submetido',
                    ]);
                }
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->errorHandler($e);
            }
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Proponentes cadastrados mas o simpósio não. Verifique novamente',
        ], 200);
    }

    protected function errorHandler(\Doctrine\DBAL\DBALException $e)
    {
        if ($e->getPrevious() && 0 === strpos($e->getPrevious()->getCode(), '23')) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Registro já existe',
            ], 200);
        }
        else {
            throw new \Exception($e->getMessage());
        }
    }

}