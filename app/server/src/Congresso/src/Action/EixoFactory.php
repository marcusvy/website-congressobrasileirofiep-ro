<?php

namespace Congresso\Action;

use Interop\Container\ContainerInterface;
use Congresso\Service\EixoService;
use Doctrine\ORM\EntityManager;


class EixoFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);
        $service = $container->get(EixoService::class);

        return new EixoAction($em, $service);
    }
}
