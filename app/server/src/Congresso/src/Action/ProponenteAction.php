<?php

namespace Congresso\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use Core\Service\ServiceInterface;
use Zend\Json\Json;

class ProponenteAction implements MiddlewareInterface
{
    private $entityManager;
    private $service;

    public function __construct(
        EntityManager $entityManager,
        ServiceInterface $service
    )
    {
        $this->entityManager = $entityManager;
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        switch ($request->getMethod()) {
            case 'OPTIONS':
            case 'GET':
                $id = $request->getAttribute('id') ? (int)$request->getAttribute('id') : 0;
                if ($id) {
                    return $this->getAction($request, $delegate);
                } else {
                    return $this->listAction($request, $delegate);
                }
            case 'POST':
                return $this->createAction($request, $delegate);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Not found!',
        ], 404);
    }

    public function getAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = ($request->getAttribute('id')) ? (int)$request->getAttribute('id') : 0;
        $entity = $this->service->get($id);
        if ($entity) {
            $result = $entity->toArray();
            return new JsonResponse([
                'success' => true,
                'eixo' => $result,
            ]);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Solicitação não encontrada!'
        ], 200);
    }

    public function listAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $collection = $this->service->list();
        return new JsonResponse([
            'success' => true,
            'collection' => $collection
        ]);
    }

    public function createAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contentType = $request->getHeader('Content-Type');
        $data = ['success' => 'false'];

        if ($contentType[0] == 'application/json') {
            $data = Json::decode($request->getBody(), Json::TYPE_ARRAY);
        }

        try {
            $entity = $this->service->create($data);
            if ($entity) {
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Eixo criado',
                ]);
            }
        } catch (\Doctrine\DBAL\DBALException $e) {
            if ($e->getPrevious() && 0 === strpos($e->getPrevious()->getCode(), '23')) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Registro já existe',
                ], 200);
            } else {
//                throw new \Exception($e->getMessage());
            }
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Registro para "%s não encontrado.',
        ], 404);
    }

}
