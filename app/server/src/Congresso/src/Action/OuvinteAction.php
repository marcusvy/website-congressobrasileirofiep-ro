<?php

namespace Congresso\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Doctrine\ORM\EntityManager;
use Core\Service\ServiceInterface;
use Zend\Json\Json;


class OuvinteAction 
    implements MiddlewareInterface
{

    private $router;
    private $entityManager;
    private $service;

    public function __construct(
        EntityManager $entityManager,
        ServiceInterface $service
    )
    {
        $this->entityManager = $entityManager;
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        switch ($request->getMethod()) {
            case 'OPTIONS':
            case 'GET':
                $id = $request->getAttribute('id') ? (int)$request->getAttribute('id') : 0;
                if ($id) {
                    return $this->getAction($request, $delegate);
                } else {
                    return $this->listAction($request, $delegate);
                }
            case 'POST':
                return $this->createAction($request, $delegate);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Not found!',
        ], 404);
    }

    public function getAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = ($request->getAttribute('id')) ? (int)$request->getAttribute('id') : 0;
        $entity = $this->service->get($id);
        if ($entity) {
            $result = $entity->toArray();
            return new JsonResponse([
                'success' => true,
                'eixo' => $result,
            ]);
        }
        return new JsonResponse([
            'success' => false,
            'message' => 'Solicitação não encontrada!'
        ], 200);
    }

    public function listAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $collection = $this->service->list();
        return new JsonResponse([
            'success' => true,
            'collection' => $collection
        ]);
    }

    public function createAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $contentType = $request->getHeader('Content-Type');
        $data = ['success' => 'false'];

        if ($contentType[0] == 'application/json') {
            $data = Json::decode($request->getBody(), Json::TYPE_ARRAY);
        }

        // try {
            $dataMap = [
                'nome' => $data['nome'],
                'situacao' => $data['situacao'],
                'instituicao_sigla' => $data['instituicao_sigla'],
                'instituicao_nome' => $data['instituicao_nome'],
                'email' => $data['email'],
                'telefoneFixo' => $data['telefone_fixo'],
                'telefoneCelular' => $data['telefone_celular'],
            ];
            $entity = $this->service->create($dataMap);
            if ($entity) {
                return new JsonResponse([
                    'success' => true,
                    'message' => 'Ouvinte submetido',
                ]);
            }
        // } catch (\Doctrine\DBAL\DBALException $e) {
        //     $this->errorHandler($e);
        // }

        return new JsonResponse([
            'success' => false,
            'message' => 'Proponentes cadastrados mas o simpósio não. Verifique novamente',
        ], 200);
    }

    protected function errorHandler(\Doctrine\DBAL\DBALException $e) {
        if ($e->getPrevious() && 0 === strpos($e->getPrevious()->getCode(), '23')) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Registro já existe',
            ], 200);
        } else {
//                throw new \Exception($e->getMessage());
        }
    }
    
}