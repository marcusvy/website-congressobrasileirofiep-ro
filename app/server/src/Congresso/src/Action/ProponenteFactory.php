<?php

namespace Congresso\Action;

use Interop\Container\ContainerInterface;
use Congresso\Service\ProponenteService;
use Doctrine\ORM\EntityManager;


class ProponenteFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);
        $service = $container->get(ProponenteService::class);

        return new ProponenteAction($em, $service);
    }
}
