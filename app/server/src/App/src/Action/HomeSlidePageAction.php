<?php

namespace App\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;


class HomeSlidePageAction implements ServerMiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
      $slides = [
        [
          'title' => '2º Enconto Regional Norte 1',
          'description' => 'Docentes em Defesa da Previdência Pública',
          'image' => '/local/slides/slide_sample.jpg'
        ]
      ];
      return new JsonResponse([
        'collection' => $slides
      ]);
    }
}
