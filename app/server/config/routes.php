<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */

$app->get('/', App\Action\HomePageAction::class, 'home');
$app->get('/api/ping', App\Action\PingAction::class, 'api.ping');


/**
 * User Module
 */
$app->route('/login', User\Action\AuthPageAction::class, ['GET', 'POST'], 'auth.login');
$app->route('/api/user[/{id:\d+}]', User\Action\UserPageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user');
$app->route('/api/user-perfil[/{id:\d+}]', User\Action\UserPerfilPageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user.perfil');
$app->route('/api/user-role[/{id:\d+}]', User\Action\UserRolePageAction::class, ['GET', 'POST', 'PUT', 'DELETE'], 'user.role');
$app->get('/api/user/activate/{id:\d+}/{key}', User\Action\ActivationPageAction::class, 'user.activation');


/**
 * Congresso Module
 */
// $app->route('/api/congresso[/{id:\d+}]', Congresso\Action\SimposioPageAction::class, ['GET', 'POST', 'PUT'], 'Congresso.Simposio');
$app->route('/api/congresso-simposio[/{id:\d+}]', Congresso\Action\SimposioAction::class, ['GET', 'POST', 'PUT','DELETE'], 'Congresso.Simposio');
$app->route('/api/congresso-simposio/activation/{id:\d+}/{status:[01]}', Congresso\Action\SimposioActivationAction::class, ['GET'], 'Congresso.Simposio.Ativacao');
$app->route('/api/congresso-eixo[/{id:\d+}]', Congresso\Action\EixoAction::class, ['GET', 'POST', 'PUT','DELETE'], 'Congresso.Eixo');
$app->route('/api/congresso-proponente[/{id:\d+}]', Congresso\Action\ProponenteAction::class, ['GET', 'POST', 'PUT','DELETE'], 'Congresso.Proponente');
$app->route('/api/congresso-ouvinte[/{id:\d+}]', Congresso\Action\OuvinteAction::class, ['GET', 'POST', 'PUT','DELETE'], 'Congresso.Ouvinte');


/**
 * Adunir
 */
$app->get('/api/adunir/slides', App\Action\HomeSlidePageAction::class,'adunir.slides');
